import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html_core/flutter_widget_from_html_core.dart'
    as core;

import 'html_widget_config.dart';
import 'html_widget_factory.dart';
typedef OnLinkTap = void Function(String url);

class HtmlWidget extends core.HtmlWidget {
  final HtmlWidgetConfig config;
  final OnLinkTap onLinkTap;

  HtmlWidget(String html,{this.onLinkTap,this.config = const HtmlWidgetConfig(),Key key,core.WidgetFactoryBuilder wfBuilder,})
      : super(
          html,
          key: key,
          wfBuilder: wfBuilder ?? (context) => HtmlWidgetFactory(context, config,onLinkTap),
        );
}
