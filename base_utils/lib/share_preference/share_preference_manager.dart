import 'package:base_utils/utils/logging_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConstantsSharePref {
  static String DB_REMIND = 'database_remind';
  static String DB_SOUND = 'database_sound';
}

enum Type { LIST, STRING, DOUBLE, INT, BOOL }

class SharePreferenceManager {
  static SharedPreferences prefs;

  static Future<void> initSharePreference() async {
    await SharedPreferences.getInstance().then((SharedPreferences data){
      prefs = data;
      data.setString("1", "1");
      log("test share :"+data.get("1"));

    });
  }

  static List<String> getPrefsListString(String key) {
    if (prefs != null && prefs.getStringList(key) != null) {
      return prefs.getStringList(key);
    } else {
      return null;
    }
  }

  static bool getPrefsBool(String key, [bool fallback = false]) {
    if (prefs != null && prefs.getBool(key) != null) {
      return prefs.getBool(key);
    } else {
      return fallback;
    }
  }
  static bool checkPrefsBool(String key) {
    if (prefs != null && prefs.get(key) != null) {
      return true;
    } else {
      return false;
    }
  }
  static SharedPreferences getPrefs (){
  return prefs;
  }
  static String getStringPrefs (String key){
    return prefs.get(key);
  }
  static void setPrefs(String key, Type type, dynamic value) {
    if (type == Type.LIST) {
      prefs.setStringList(key, value);
    } else if (type == Type.STRING) {
      prefs.setString(key, value);
    } else if (type == Type.DOUBLE) {
      prefs.setDouble(key, value);
    } else if (type == Type.INT) {
      prefs.setInt(key, value);
    } else if (type == Type.BOOL) {
      prefs.setBool(key, value);
    }
  }

  static void removePrefs(String key) {
    prefs.remove(key);
  }
}
