import 'dart:io';
import 'dart:ui';

import 'package:base_utils/utils/string_utils.dart';
import 'package:clone_local_notifications/flutter_local_notifications.dart';
import 'package:http/http.dart' as http;
import 'package:path_provider/path_provider.dart';
import 'package:image/image.dart';

class NotificationManager {

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
  new FlutterLocalNotificationsPlugin();

  NotificationManager() {
    initNotifications();
  }

  factory NotificationManager.initial() => NotificationManager();

  void initNotifications() {
    var initializationSettingsAndroid =
    new AndroidInitializationSettings('ic_launcher');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(
        initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings,
        selectNotification: null);
  }

  Future<String> _downloadAndSaveImage(String url, String fileName) async {
    var directory = await getApplicationDocumentsDirectory();
    var filePath = '${directory.path}/$fileName';
    var response = await http.get(url);
    var file = new File(filePath);
    await file.writeAsBytes(response.bodyBytes);
    Image image = decodeImage(new File(filePath).readAsBytesSync());
    Image thumbnail = copyResize(image, 480);
    new File('${directory.path}/$fileName')
      ..writeAsBytesSync(encodePng(thumbnail));
    return filePath;
  }

  void show({
    String title,
    String body,
    int notificationId = 0,
    String deepLink,
    bool isVira = true,
    bool isSoundf=true,
    ///sau bao lau thi se push (millis)
    int afterTime = 0,
    String bigPictureUrl,
    bool test = false,
  }) async {
    var platformChannelSpecifics;

    if (isNotEmpty(bigPictureUrl) && Platform.isAndroid) {
      //big style

      //save image to file.
      var bigPicturePath = await _downloadAndSaveImage(
          bigPictureUrl, 'bigPicture');

      var bigPictureStyleInformation = new BigPictureStyleInformation(
        bigPicturePath,
        BitmapSource.FilePath,
        largeIconBitmapSource: BitmapSource.FilePath,
        contentTitle: title,
        summaryText: body,
        htmlFormatContentTitle: false,
        htmlFormatSummaryText: false,
      );
      var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
          'big text channel id',
          'big text channel name',
          'big text channel description',
          style: AndroidNotificationStyle.BigPicture,
          styleInformation: bigPictureStyleInformation);

      platformChannelSpecifics =
      new NotificationDetails(androidPlatformChannelSpecifics, null);
    } else {
      //normal
      var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
        'your other channel id',
        'your other channel name',
        'your other channel description',
        playSound: isSoundf,
        enableVibration:isVira,);
      var iosPlatformChannelSpecifics = new IOSNotificationDetails();
      platformChannelSpecifics = new NotificationDetails(
          androidPlatformChannelSpecifics, iosPlatformChannelSpecifics);
    }

    if (afterTime > 0) {
      var scheduledNotificationDateTime = new DateTime.now()
          .add(new Duration(milliseconds: test ? 5000 : afterTime));
      await flutterLocalNotificationsPlugin.schedule(notificationId, title,
          body, scheduledNotificationDateTime, platformChannelSpecifics,
          payload: deepLink);
    } else {
      await flutterLocalNotificationsPlugin.show(
          notificationId, title, body, platformChannelSpecifics,
          payload: deepLink);
    }
  }
}
