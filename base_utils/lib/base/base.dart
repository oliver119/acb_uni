import 'dart:typed_data';


import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/services.dart';
import 'package:flutter_advanced_networkimage/flutter_advanced_networkimage.dart';
//import 'package:flutter_advanced_networkimage/provider.dart';


/// Provides common utilities and functions to build ui and handle app lifecycle
abstract class BaseState<T extends StatefulWidget> extends LifeCycleState<T> {
  String packageName = "abc_uni";

  Size screenSize;


  Size designScreenSize;

  /// Called when the app is temporarily closed or a new route is pushed
  @override
  void onPause() {}

  /// Called when users return to the app or the adjacent route of this widget is popped
  @override
  void onResume() {}

  /// Called onnce when this state's widget finished building
  @override
  void onFirstFrame() {}

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    screenSize = MediaQuery.of(context).size;
    designScreenSize = getDesignSize() ?? Size(375.0, 812.0);
  }

  /// Override to define new design size for ui
  Size getDesignSize() => null;

  /// Scale the provided 'designSize' proportionally to the screen's width
  double scaleWidth(num designSize) {
    if (designSize == null) return null;
    return screenSize.width * designSize / designScreenSize.width;
  }

  /// Scale the provided 'designSize' proportionally to the screen's height
  double scaleHeight(num designSize) {
    if (designSize == null) return null;
    return screenSize.height * designSize / designScreenSize.height;
  }

  Widget buildAssetsImage(String path,
      {BoxFit fit = BoxFit.contain, num width, num height, String package}) {
    return Image.asset(path,
        height: scaleHeight(height),
        width: scaleWidth(width),
        package: package ?? packageName,
        fit: fit);
  }

  DecorationImage buildDecorationImage(String path,
      {BoxFit fit = BoxFit.contain, ColorFilter filter, String package}) {
    return DecorationImage(
        image: AssetImage(path, package: package ?? packageName),
        fit: fit,
        colorFilter: filter);
  }

  Widget buildRemoteImage(
    String url, {
    num height,
    num width,
    BoxFit fit = BoxFit.contain,
    double scale = 1.0,
    Map<String, String> header,
    bool useDiskCache = false,
    Function loadedCallback,
    Function loadFailedCallback,
    Uint8List fallbackImage,
  }) {
    return Image(
      height: height,
      width: width,
      fit: fit,
      image: AdvancedNetworkImage(
        url,
        scale: scale,
        fallbackImage: fallbackImage,
        header: header,
        loadedCallback: loadedCallback,
        loadFailedCallback: loadFailedCallback,
        useDiskCache: useDiskCache,
        timeoutDuration: Duration(seconds: 30),
        retryLimit: 10,
        retryDuration: Duration(milliseconds: 2800),
      ),
    );
  }

//  @override
//  RouteObserver<Route> get routeObserver => Router.instance.routeObserver;
}

abstract class LifeCycleState<T extends StatefulWidget> extends State<T>
    with RouteAware {
  @override
  void initState() {
    super.initState();
    SystemLifecycleListener.instance.addCallback(handleLifeCycleCallback);

    SchedulerBinding.instance.addPostFrameCallback((_) {
      onFirstFrame();
    });
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    routeObserver.subscribe(this, ModalRoute.of(context));
  }

  void handleLifeCycleCallback(AppLifeCycle lifeCycle) {
    if (lifeCycle == AppLifeCycle.onResume) {
      onResume();
    } else if (lifeCycle == AppLifeCycle.onPause) {
      onPause();
    }
  }

  @override
  void dispose() {
    routeObserver.unsubscribe(this);
    SystemLifecycleListener.instance.removeCallback(handleLifeCycleCallback);
    super.dispose();
  }

  void onResume() {}

  void onPause() {}

  void onFirstFrame() {}

  RouteObserver<Route> get routeObserver;

  @override
  void didPopNext() {
    SystemLifecycleListener.instance.addCallback(handleLifeCycleCallback);
    onResume();
  }

  @override
  void didPushNext() {
    SystemLifecycleListener.instance.removeCallback(handleLifeCycleCallback);
    onPause();
  }

  @override
  void didPop() {}

  @override
  void didPush() {}
}

class SystemLifecycleListener {
  static final instance = SystemLifecycleListener();

  final _callbacks = Set<AppLifeCycleCallback>();

  void _invokeCallbacks(AppLifeCycle lifeCycle) {
    for (AppLifeCycleCallback callback in _callbacks) {
      callback(lifeCycle);
    }
  }

  void addCallback(AppLifeCycleCallback callback) {
    _callbacks.add(callback);
  }

  void removeCallback(AppLifeCycleCallback callback) {
    _callbacks.remove(callback);
  }

  SystemLifecycleListener() {
    setHandler();
  }

  void setHandler() {
    SystemChannels.lifecycle.setMessageHandler((msg) {
      if (msg.toString() == AppLifecycleState.resumed.toString()) {
        _invokeCallbacks(AppLifeCycle.onResume);
      } else if (msg.toString() == AppLifecycleState.paused.toString()) {
        _invokeCallbacks(AppLifeCycle.onPause);
      } else if (msg.toString() == AppLifecycleState.inactive.toString()) {
//        print("onInactive");
      } else if (msg.toString() == AppLifecycleState.suspending.toString()) {
//        print("onSuspend");
      }
    });
  }
}

enum AppLifeCycle { onPause, onResume }

typedef AppLifeCycleCallback = void Function(AppLifeCycle lifeCycle);
