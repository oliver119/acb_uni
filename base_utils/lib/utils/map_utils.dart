Map<String, String> convertFrom(Map<String, dynamic> map) {
  if (map != null) {
    Map<String, String> _params = {};
    map.forEach((k, v) {
      _params[k] = v.toString();
    });
    return _params;
  }
  return null;
}
