import 'package:base_utils/utils/string_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void copyToClipboard(String text, {String successMessage, BuildContext context}) {
  if (isNotEmpty(text)) {
    Clipboard.setData(new ClipboardData(text: text));
    if(context != null){
      Scaffold.of(context)?.showSnackBar(new SnackBar(
          duration: Duration(seconds: 1),
          content: new Text(successMessage ?? "Sao chép thành công.")));
    }
  }
}
