import 'package:flutter/material.dart';

class AssetsColor {
  static const greyError = Color(0xFF8e8e8e);

  static const retryButtonColor = Color(0xFFe5101d);

  static const sendoRed = Color(0xffe5101d);

  static const titleRed = Color(0xffd0021b);

  static const defaultActiveTabBar = Color(0xffa50c0c);

  static const defaultTabBar = Color(0xffFF302D);

  static const greyLoading = Color(0xFFc3c3c3);

  static const transparent = Colors.transparent;

  static const black = Colors.black;

  static const red = Color(0xFFe5101d);

  static const green = Color(0xFF38761d);

  static const redAccent = Color(0xFFd0021b);

  static const greenAccent = Color(0xFF417505);

  static const grey = Color(0xFF686868);

  static const white = Colors.white;

  static const transAppBarButtonBackground = Color(0x2a222222);

  static var transparentGrey = Color(0x90cecece);
}
