class AssetsImage {
  static const toTopImageAsset = 'assets/ic_to_top.png';

  static const error = "assets/sad.svg";

  static const bannerPlaceholder = "assets/banner_placeholder.png";
}
