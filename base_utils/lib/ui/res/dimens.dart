import 'package:flutter/material.dart';

class AssetsDimen {
  static const toTopButtonMarginRight = 12.0;

  static const toTopButtonMarginBottom = 47.0;

  static const toTopButtonSize = 45.0;

  static const appBarHeightIOS = 45.0;

  static const titleSize = 18.0;

  static const titleMargin = EdgeInsets.fromLTRB(12.0, 23.0, 6.0, 6.0);

  static const loadingIndicatorPadding = EdgeInsets.all(16.0);

  static const appBarHeightAndroid = 50.0;

  static const itemDivider = 1.0;

  static const fontSizeSmall = 9.0;

  static const fontSizeMedium = 11.0;

  static const fontSizeNormal = 14.0;

  static const fontSizeLarge = 16.0;

  static const fontSizeSuperLarge = 18.0;

  static const fontSizeHuge = 20.0;

  static const margin4 = 4.0;

  static const margin5 = 5.0;

  static const margin8 = 8.0;

  static const margin10 = 10.0;

  static const margin12 = 12.0;

  static const margin14 = 14.0;

  static const margin15 = 15.0;

  static const margin16 = 16.0;

  static const margin18 = 18.0;

  static const margin40 = 40.0;

  static const padding8 = 8.0;

  static const padding12 = 12.0;

  static const padding14 = 14.0;

  static const padding16 = 16.0;

  static const padding18 = 18.0;

  static const padding20 = 20.0;

  static const defaultElevation = 8.0;
}
