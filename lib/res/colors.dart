import 'dart:ui';

import 'package:flutter/material.dart';

class AssetsColor {

  static const  primaryColor = Color(0xFF4aa0d5);
  static const  subColor = Color(0xFF333333);

}
