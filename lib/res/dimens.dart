import 'package:flutter/material.dart';

class AssetsDimen {
  static const toTopButtonMarginRight = 12.0;

  static const ProductSliderSize = 380.0;

  static const toTopButtonMarginBottom = 47.0;

  static const titleSize = 18.0;

  static const titleMargin = EdgeInsets.fromLTRB(12.0, 23.0, 6.0, 6.0);

  static const headerDividerMargin = EdgeInsets.only(bottom: 4.0);

  static const headerDividerHeight = 3.0;

  static const eventExpiredTextSize = 10.0;

  static const durationLabelMargin = EdgeInsets.only(top: 8.0);

  static const durationMargin = EdgeInsets.only(top: 3.0);

  static const leftEventMargin = EdgeInsets.fromLTRB(12.0, 18.0, 6.0, 0.0);

  static const rightEventMargin = EdgeInsets.fromLTRB(6.0, 18.0, 12.0, 0.0);

  static const durationTextSize = 11.0;

  static const loadingIndicatorPadding = EdgeInsets.all(16.0);

  static const toTopButtonSize = 45.0;

  static const appBarHeightIOS = 45.0;

  static const appBarHeightAndroid = 50.0;

  // flash sale
  static const height0 = 0.0;

  static const itemDivider = 1.0;

  static const fontSizeSmall = 9.0;

  static const fontSizeMedium = 11.0;

  static const fontSizeNormal = 14.0;

  static const fontSizeLarge = 16.0;

  static const fontSizeSuperLarge = 18.0;

  static const fontSizeHuge = 20.0;

  static const margin4 = 4.0;

  static const margin5 = 5.0;

  static const margin8 = 8.0;

  static const margin10 = 10.0;

  static const margin12 = 12.0;

  static const margin14 = 14.0;

  static const margin15 = 15.0;

  static const margin16 = 16.0;

  static const margin18 = 18.0;

  static const margin40 = 40.0;

  static const pading8 = 8.0;

  static const pading12 = 12.0;

  static const pading14 = 14.0;

  static const pading16 = 16.0;

  static const pading18 = 18.0;

  static const pading20 = 20.0;

  static const pading25 = 25.0;

  static const fontSizeTitleTab = fontSizeMedium;

  static const fontSizeTimeTab = fontSizeNormal;

  static const heightContainerTabBar = 48.0;

  static const widthContainerTabbar = double.infinity;

  static const weightIndicatorTabBar = 1.0;

  static const widthContainerTabHeader = 60.0;

  static const widthImageBackgroundLoadingProduct = 120.0;

  static const heightImageBackgroundLoadingProduct = 120.0;

  static const widthImageLoadingProduct = 60.0;

  static const heightImageLoadingProduct = 60.0;

  static const heightCountDown = 48.0;

  static const heightContainerListProductItem = 125.0;

  static const heightImageSenmall = 23.0;

  static const wightImageSenmall = 33.0;

  static const widthContainerWhite = 86.0;

  static const widthIconImageSale = 40.0;

  static const rightPositionedBannerDiscount = -2.0;

  static const topPositionedBannerDiscount = -1.5;

  static const topPositionedTextDiscount = 4.0;

  static const leftPositionedTextDiscount = 3.3;

  static const heightProgressBar = 30.0;

  static const widthProgressBar = 10.0;

  static const borderRadiusContainerBackgroundProgressBar = 15.0;

  static const borderRadiusImageBurn = 5.0;

  static const heightContainerCountDown = 50.0;

  static const rightPaddingTimerCotain = 8.0;

  static const leftPaddingTimerCotain = 8.0;

  static const widthContainCountDown = 25.0;

  static const heightContainCountDown = 20.0;

  static const borderRadiusContainCountDown = 4.0;

  static const widthContainerTwoDot = 10.0;

  static const alertButtonHeight = 45.0;

  static const productItemButtonHeight = 32.0;

  static const productItemButtonMinWidth = 86.0;

  static const soldOutIconSize = 86.0;

  static double itemHotNewMargin = 5.0;

  static EdgeInsets itemHotNewPadding = EdgeInsets.fromLTRB(7.0, 0.0, 7.0, 0.0);

  static var loadMoreButtonWidth = 137.0;

  static var loadMoreButtonHeight = 31.0;

  static var hashTagRowMargin = EdgeInsets.only(top: 15.0, bottom: 15.0);

  static var listDefaultPadding = EdgeInsets.only(left: 2.5, right: 2.5);

  static var heightIconImageSale = 24.0;

  static double productTemplateTabBarHeight = 45.0;

  static double loadMoreButtonMargin = 20.0;

  static double hashTagRowHeight = 15.0 * 3;

  static var tabIndicatorWeight = 2.0;

  static var tabIndicatorMargin = EdgeInsets.only(left: 16.0, right: 16.0);

  static var productTabMinWidth = 100.0;

  static var productListBottomPadding = 8.0;

  static var productTabBottomPadding = 20.0;

  static var productListBottomShrink = -15.0;

  static var productListLoadingPageHeight = 375.0;

  static var rewardItemRatio = 2.1625;

  static var buttonSize = 21.5;

  static var rewardItemMargin = 12.0;

  static var rewardPagePadding = 14.0;

  static var eventDetailListBottmPadding = 48.0;

  static var eventSaleImage = 52.0;

  static var aarPageMargin = EdgeInsets.only(left: 15.0, right: 15.0);

  static double aarRoundedCornerRadius = 7.0;

  static double productItemCornerRadius = 6.0;

  static var rankingPageMargin = EdgeInsets.only(left: 12.0, right: 12.0);

  static num ticketItemHeight = 46.0;

  static num ticketItemExtent = 8.0;

  static var loadingIndicatorSize = 25.0;

  /*** Generic Item Margins ***/

  static double itemMargin = 2.0;

  static double itemMargin2 = itemMargin * 2;

  static double itemMargin3 = itemMargin * 3;

  static double itemMargin4 = itemMargin * 4;

  static double itemMargin5 = itemMargin * 5;

  static double itemMargin6 = itemMargin * 6;

  static double itemMargin7 = itemMargin * 7;

  static double itemMargin8 = itemMargin * 8;

  static double itemMargin9 = itemMargin * 9;

  static double itemMargin10 = itemMargin * 10;

  static double itemMargin15 = itemMargin * 15;

  static double itemMargin13 = itemMargin * 13;

  static var rclxHomeIconHeight = 44.0;

  static double rclxIconSize = 28.0;

  static num coinTopDialogHeight = 74.0;

  static int lightningTopMargin = 30;

  static int get logoBrandImageSize => 27;
  static num lmxIconSize = 35;
}
