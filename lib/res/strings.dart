import 'package:base_utils/utils/string_utils.dart';

class AssetsString {

  static const String groupName = 'Student';
  static const String menu_noti = 'Notification';
  static const String menu_logout = 'Logout';
  static const String menu_profile = 'Profile';
  static const String menu_change_pass = 'Changepass';
  static const String menu_timeable = 'Timetable';
  static const String menu_aten = 'Check atention';
  static const String menu_statis = 'Statistical';
  static const String menu_settings = 'Settings';

  //

  static const String set_vira = 'set_vira';
  static const String set_sound = 'set_sound';
  static const String set_time = 'set_time';
}
