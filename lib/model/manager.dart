import 'package:acb_uni_student/model/timeable.dart';
import 'package:acb_uni_student/model/menu_data.dart';
import 'package:acb_uni_student/model/user_info.dart';

class Manager {
  static final Manager instance = new Manager._internal();

  ManagerData data;
  UserInfo mUserInfo;
  Timeable mTimeableData;

  factory Manager() {
    return instance;
  }

  Manager._internal() {
    this.data = ManagerData();
  }
  ManagerData getManagerData(){

    return this.data;
  }
  void setUserInfo(UserInfo userInfo){
    mUserInfo = userInfo;

  }
  UserInfo getUserInfo(){
   return mUserInfo;

  }

  void setTimeable(Timeable timeable){
    mTimeableData = timeable;

  }
  Timeable getTimeable(){
    return mTimeableData;

  }

}
class ManagerData {
  //MerchantUserInfo _userInfo;



  ManagerData();

  List<MenuDataItem> get menuInfo => MenuData().getMenuData();
  List<MenuDataItem> get menuInfoPro => MenuData().getMenuDataPro();
//  set userInfo(MerchantUserInfo value) {
//    _userInfo = value;
//  }


  //get userToken => _userInfo != null ? _userInfo.accessToken : null;
  //get userToken => _userInfo != null ? _userInfo.accessToken : null;
}

//Future<MerchantUserInfo> getUserInfo() async {
//  MerchantUserInfo ui;
//  if (Router.instance.dev) {
//    await Injector.provideUserService().login(DataRequest(
//        onSuccess: (res) {
//          ui = MerchantUserInfo.fromJson(res);
//        },
//        onFailure: (e) {}));
//  } else {
//    ui = await actionGetSendoUserInfo();
//  }
//  return ui;
//}