import 'package:acb_uni_student/res/res.dart';
import 'package:flutter/material.dart';

class MenuData {
  List<MenuDataItem> data = List();
  List<MenuDataItem> dataPro = List();

  MenuData(){
    // ignore: argument_type_not_assignable
//    MenuDataItem item = new MenuDataItem(id: MenuType.NOTI,name: AssetsString.menu_noti,icon: Icons.notifications_active);
//    MenuDataItem item = new MenuDataItem(id: MenuType.NOTI);



   // data.add(new MenuDataItem(id: MenuType.NOTI,name: AssetsString.menu_noti,icon: Icons.notifications_active));
   // data.add(new MenuDataItem(id: MenuType.PROFILE,name: AssetsString.menu_profile,icon: Icons.person));
    //data.add(new MenuDataItem(id: MenuType.CHANGE_PASS,name: AssetsString.menu_change_pass,icon: Icons.lock));
    data.add(new MenuDataItem(id: MenuType.TIME_ABLE,name: AssetsString.menu_timeable,icon: Icons.event_note));
    data.add(new MenuDataItem(id: MenuType.CHECK_ATEN,name: AssetsString.menu_aten,icon: Icons.check));
    data.add(new MenuDataItem(id: MenuType.SETTINGS,name: AssetsString.menu_settings,icon: Icons.settings));
    data.add(new MenuDataItem(id: MenuType.LOG_OUT,name: AssetsString.menu_logout,icon: Icons.exit_to_app));

    dataPro.add(new MenuDataItem(id: MenuType.TIME_ABLE_PRO,name: AssetsString.menu_timeable,icon: Icons.event_note));
    dataPro.add(new MenuDataItem(id: MenuType.STATIS,name: AssetsString.menu_statis,icon: Icons.insert_chart));
    dataPro.add(new MenuDataItem(id: MenuType.SETTINGS,name: AssetsString.menu_settings,icon: Icons.settings));
    dataPro.add(new MenuDataItem(id: MenuType.LOG_OUT,name: AssetsString.menu_logout,icon: Icons.exit_to_app));

  }

  List<MenuDataItem> getMenuData(){
    return this.data;
  }

  List<MenuDataItem> getMenuDataPro(){
    return this.dataPro;
  }

//  MenuData({
//    this.data,
//  });
}
class MenuType {
 static const String NOTI = "NOTI";
 static const String TIME_ABLE = "TIME_ABLE";
 static const String TIME_ABLE_PRO = "TIME_TABLE_PRO";
 static const String CHECK_ATEN = "CHECK_ATEN";
 static const String STATIS = "STATIS";
 static const String LOG_OUT = "LOG_OUT";
 static const String SETTINGS = "SETTINGS";
 static const String PROFILE = "PROFILE";
 static const String CHANGE_PASS = "CHANGE_PASS";

}
class MenuDataItem {
  String id;
  String name;
  IconData icon;

  MenuDataItem({
    this.id,
    this.name,
    this.icon,
  });
}
