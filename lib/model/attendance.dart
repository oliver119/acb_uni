// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

Attendance welcomeFromJson(String str) => Attendance.fromJson(json.decode(str));

String welcomeToJson(Attendance data) => json.encode(data.toJson());

class Attendance {
  List<Report> reports;
  double percentageFinishClasses;

  Attendance({
    this.reports,
    this.percentageFinishClasses,
  });

  factory Attendance.fromJson(Map<String, dynamic> json) => new Attendance(
    reports: json["Reports"] == null ? null : new List<Report>.from(json["Reports"].map((x) => Report.fromJson(x))),
    percentageFinishClasses: json["percentageFinishClasses"] == null ? null : json["percentageFinishClasses"].toDouble(),
  );

  Map<String, dynamic> toJson() => {
    "Reports": reports == null ? null : new List<dynamic>.from(reports.map((x) => x.toJson())),
    "percentageFinishClasses": percentageFinishClasses == null ? null : percentageFinishClasses,
  };
}

class Report {
  String module;
  int allClasses;
  int finishedClasses;
  int absentClasses;
  int presentClasses;
  List<dynamic> present;
  List<DateTime> absent;
  List<DateTime> all;
  List<DateTime> finish;

  Report({
    this.module,
    this.allClasses,
    this.finishedClasses,
    this.absentClasses,
    this.presentClasses,
    this.present,
    this.absent,
    this.all,
    this.finish,
  });

  factory Report.fromJson(Map<String, dynamic> json) => new Report(
    module: json["Module"] == null ? null : json["Module"],
    allClasses: json["AllClasses"] == null ? null : json["AllClasses"],
    finishedClasses: json["FinishedClasses"] == null ? null : json["FinishedClasses"],
    absentClasses: json["AbsentClasses"] == null ? null : json["AbsentClasses"],
    presentClasses: json["PresentClasses"] == null ? null : json["PresentClasses"],
    present: json["Present"] == null ? null : new List<dynamic>.from(json["Present"].map((x) => x)),
    absent: json["Absent"] == null ? null : new List<DateTime>.from(json["Absent"].map((x) => DateTime.parse(x))),
    all: json["All"] == null ? null : new List<DateTime>.from(json["All"].map((x) => DateTime.parse(x))),
    finish: json["Finish"] == null ? null : new List<DateTime>.from(json["Finish"].map((x) => DateTime.parse(x))),
  );

  Map<String, dynamic> toJson() => {
    "Module": module == null ? null : module,
    "AllClasses": allClasses == null ? null : allClasses,
    "FinishedClasses": finishedClasses == null ? null : finishedClasses,
    "AbsentClasses": absentClasses == null ? null : absentClasses,
    "PresentClasses": presentClasses == null ? null : presentClasses,
    "Present": present == null ? null : new List<dynamic>.from(present.map((x) => x)),
    "Absent": absent == null ? null : new List<dynamic>.from(absent.map((x) => x.toIso8601String())),
    "All": all == null ? null : new List<dynamic>.from(all.map((x) => x.toIso8601String())),
    "Finish": finish == null ? null : new List<dynamic>.from(finish.map((x) => x.toIso8601String())),
  };
}
