import 'dart:convert';

UserInfo UserInfoFromJson(String str) {
  final jsonData = json.decode(str);
  return UserInfo.fromJson(jsonData);
}

String userInfoToJson(UserInfo data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class UserInfo {
  String accessToken;
  String tokenType;
  int expiresIn;
  String clientId;
  String roleId;
  String firstName;
  String lastName;
  String groupId;
  String email;
  String id;
  String lastUpdated;
  String issued;
  String expires;
  String errorDescription;

  UserInfo({
    this.accessToken,
    this.tokenType,
    this.expiresIn,
    this.clientId,
    this.roleId,
    this.firstName,
    this.lastName,
    this.groupId,
    this.email,
    this.id,
    this.lastUpdated,
    this.issued,
    this.expires,
    this.errorDescription
  });

  factory UserInfo.fromJson(Map<String, dynamic> json) => new UserInfo(
    accessToken: json["access_token"] == null ? null : json["access_token"],
    tokenType: json["token_type"] == null ? null : json["token_type"],
    expiresIn: json["expires_in"] == null ? null : json["expires_in"],
    clientId: json["client_id"] == null ? null : json["client_id"],
    roleId: json["RoleId"] == null ? null : json["RoleId"],
    firstName: json["FirstName"] == null ? null : json["FirstName"],
    lastName: json["LastName"] == null ? null : json["LastName"],
    groupId: json["GroupId"] == null ? null : json["GroupId"],
    email: json["Email"] == null ? null : json["Email"],
    id: json["Id"] == null ? null : json["Id"],
    lastUpdated: json["LastUpdated"] == null ? null : json["LastUpdated"],
    issued: json[".issued"] == null ? null : json[".issued"],
    expires: json[".expires"] == null ? null : json[".expires"],
    errorDescription: json["error_description"] == null ? null : json["error_description"],
  );

  Map<String, dynamic> toJson() => {
    "access_token": accessToken == null ? null : accessToken,
    "token_type": tokenType == null ? null : tokenType,
    "expires_in": expiresIn == null ? null : expiresIn,
    "client_id": clientId == null ? null : clientId,
    "RoleId": roleId == null ? null : roleId,
    "FirstName": firstName == null ? null : firstName,
    "LastName": lastName == null ? null : lastName,
    "GroupId": groupId == null ? null : groupId,
    "Email": email == null ? null : email,
    "Id": id == null ? null : id,
    "LastUpdated": lastUpdated == null ? null : lastUpdated,
    ".issued": issued == null ? null : issued,
    ".expires": expires == null ? null : expires,
    "error_description":errorDescription == null ? null : errorDescription,
  };
}