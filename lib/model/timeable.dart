import 'dart:convert';

Timeable welcomeFromJson(String str) {
  final jsonData = json.decode(str);
  return Timeable.fromJson(jsonData);
}

String welcomeToJson(Timeable data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

class Timeable {
  List<DateTime> allClassDates;
  List<AllClasess> allClasess;

  Timeable({
    this.allClassDates,
    this.allClasess,
  });

  factory Timeable.fromJson(Map<String, dynamic> json) => new Timeable(
        allClassDates: json["AllClassDates"] == null
            ? null
            : new List<DateTime>.from(
                json["AllClassDates"].map((x) => DateTime.parse(x))),
        allClasess: json["AllClasess"] == null
            ? null
            : new List<AllClasess>.from(
                json["AllClasess"].map((x) => AllClasess.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "AllClassDates": allClassDates == null
            ? null
            : new List<AllClasess>.from(
                allClassDates.map((x) => x.toIso8601String())),
        "AllClasess": allClasess == null
            ? null
            : new List<AllClasess>.from(allClasess.map((x) => x.toJson())),
      };
}

class AllClasess {
  int id;
  RoomId roomId;
  ModuleId moduleId;
  ProfessorId professorId;
  TimeSlotId timeSlotId;
  DateTime date;
  dynamic note;
  IsCanceled isCanceled;

  AllClasess({
    this.id,
    this.roomId,
    this.moduleId,
    this.professorId,
    this.timeSlotId,
    this.date,
    this.note,
    this.isCanceled,
  });

  factory AllClasess.fromJson(Map<String, dynamic> json) => new AllClasess(
        id: json["Id"] == null ? null : json["Id"],
        roomId: json["RoomId"] == null ? null : RoomId.fromJson(json["RoomId"]),
        moduleId: json["ModuleId"] == null
            ? null
            : ModuleId.fromJson(json["ModuleId"]),
        professorId: json["ProfessorId"] == null
            ? null
            : ProfessorId.fromJson(json["ProfessorId"]),
        timeSlotId: json["TimeSlotId"] == null
            ? null
            : TimeSlotId.fromJson(json["TimeSlotId"]),
        date: json["Date"] == null ? null : DateTime.parse(json["Date"]),
        note: json["Note"],
        isCanceled: json["IsCanceled"] == null
            ? null
            : IsCanceled.fromJson(json["IsCanceled"]),
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "RoomId": roomId == null ? null : roomId.toJson(),
        "ModuleId": moduleId == null ? null : moduleId.toJson(),
        "ProfessorId": professorId == null ? null : professorId.toJson(),
        "TimeSlotId": timeSlotId == null ? null : timeSlotId.toJson(),
        "Date": date == null ? null : date.toIso8601String(),
        "Note": note,
        "IsCanceled": isCanceled == null ? null : isCanceled.toJson(),
      };
}

class ClassGet {
  int id;
  String groupCode;
  String moduleName;
  String roomName;
  String timeSlotName;
  String professorName;
  DateTime date;
  String note;

  ClassGet({
    this.id,
    this.groupCode,
    this.moduleName,
    this.roomName,
    this.timeSlotName,
    this.professorName,
    this.date,
    this.note,
  });

  factory ClassGet.fromJson(Map<String, dynamic> json) => new ClassGet(
    id: json["Id"] == null ? null : json["Id"],
    groupCode: json["GroupCode"] == null ? null : json["GroupCode"],
    moduleName: json["ModuleName"] == null ? null : json["ModuleName"],
    roomName: json["RoomName"] == null ? null : json["RoomName"],
    timeSlotName: json["TimeSlotName"] == null ? null : json["TimeSlotName"],
    professorName: json["ProfessorName"] == null ? null : json["ProfessorName"],
    date: json["Date"] == null ? null : DateTime.parse(json["Date"]),
    note: json["Note"] == null ? null : json["Note"],
  );

  Map<String, dynamic> toJson() => {
    "Id": id == null ? null : id,
    "GroupCode": groupCode == null ? null : groupCode,
    "ModuleName": moduleName == null ? null : moduleName,
    "RoomName": roomName == null ? null : roomName,
    "TimeSlotName": timeSlotName == null ? null : timeSlotName,
    "ProfessorName": professorName == null ? null : professorName,
    "Date": date == null ? null : date.toIso8601String(),
    "Note": note == null ? null : note,
  };
}

class IsCanceled {
  bool isCanceled;
  bool isNewClass;
  ClassGet oldClass;
  ClassGet newClass;

  IsCanceled({
    this.isCanceled,
    this.isNewClass,
    this.oldClass,
    this.newClass,
  });

  factory IsCanceled.fromJson(Map<String, dynamic> json) => new IsCanceled(
        isCanceled: json["isCanceled"] == null ? null : json["isCanceled"],
        isNewClass: json["isNewClass"] == null ? null : json["isNewClass"],
        oldClass:
            json["oldClass"] == null ? null : ClassGet.fromJson(json["oldClass"]),
        newClass:
            json["newClass"] == null ? null : ClassGet.fromJson(json["newClass"]),
      );

  Map<String, dynamic> toJson() => {
        "isCanceled": isCanceled == null ? null : isCanceled,
        "isNewClass": isNewClass == null ? null : isNewClass,
        "oldClass": oldClass == null ? null : oldClass.toJson(),
        "newClass": newClass == null ? null : newClass.toJson(),
      };
}

class ModuleId {
  int id;
  String code;
  String name;
  int numberOfClass;

  ModuleId({
    this.id,
    this.code,
    this.name,
    this.numberOfClass,
  });

  factory ModuleId.fromJson(Map<String, dynamic> json) => new ModuleId(
        id: json["Id"] == null ? null : json["Id"],
        code: json["Code"] == null ? null : json["Code"],
        name: json["Name"] == null ? null : json["Name"],
        numberOfClass:
            json["NumberOfClass"] == null ? null : json["NumberOfClass"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "Code": code == null ? null : code,
        "Name": name == null ? null : name,
        "NumberOfClass": numberOfClass == null ? null : numberOfClass,
      };
}

class ProfessorId {
  int id;
  String email;
  String firstName;
  String lastName;
  String password;
  int groupId;
  int roleId;
  DateTime lastUpdated;

  ProfessorId({
    this.id,
    this.email,
    this.firstName,
    this.lastName,
    this.password,
    this.groupId,
    this.roleId,
    this.lastUpdated,
  });

  factory ProfessorId.fromJson(Map<String, dynamic> json) => new ProfessorId(
        id: json["Id"] == null ? null : json["Id"],
        email: json["Email"] == null ? null : json["Email"],
        firstName: json["FirstName"] == null ? null : json["FirstName"],
        lastName: json["LastName"] == null ? null : json["LastName"],
        password: json["Password"] == null ? null : json["Password"],
        groupId: json["GroupId"] == null ? null : json["GroupId"],
        roleId: json["RoleId"] == null ? null : json["RoleId"],
        lastUpdated: json["LastUpdated"] == null
            ? null
            : DateTime.parse(json["LastUpdated"]),
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "Email": email == null ? null : email,
        "FirstName": firstName == null ? null : firstName,
        "LastName": lastName == null ? null : lastName,
        "Password": password == null ? null : password,
        "GroupId": groupId == null ? null : groupId,
        "RoleId": roleId == null ? null : roleId,
        "LastUpdated":
            lastUpdated == null ? null : lastUpdated.toIso8601String(),
      };
}

class RoomId {
  int id;
  String name;
  int capacity;

  RoomId({
    this.id,
    this.name,
    this.capacity,
  });

  factory RoomId.fromJson(Map<String, dynamic> json) => new RoomId(
        id: json["Id"] == null ? null : json["Id"],
        name: json["Name"] == null ? null : json["Name"],
        capacity: json["Capacity"] == null ? null : json["Capacity"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "Name": name == null ? null : name,
        "Capacity": capacity == null ? null : capacity,
      };
}


class TimeSlotId {
  int id;
  String name;
  int time;

  TimeSlotId({
    this.id,
    this.name,
    this.time,
  });

  factory TimeSlotId.fromJson(Map<String, dynamic> json) => new TimeSlotId(
        id: json["Id"] == null ? null : json["Id"],
        name: json["Name"] == null ? null : json["Name"],
        time: json["Time"] == null ? null : json["Time"],
      );

  Map<String, dynamic> toJson() => {
        "Id": id == null ? null : id,
        "Name": name == null ? null : name,
        "Time": time == null ? null : time,
      };
}
class Module {
  int id;
  String code;
  String name;
  int numberOfClass;

  Module({
    this.id,
    this.code,
    this.name,
    this.numberOfClass,
  });

  factory Module.fromJson(Map<String, dynamic> json) => new Module(
    id: json["Id"] == null ? null : json["Id"],
    code: json["Code"] == null ? null : json["Code"],
    name: json["Name"] == null ? null : json["Name"],
    numberOfClass:
    json["NumberOfClass"] == null ? null : json["NumberOfClass"],
  );

  Map<String, dynamic> toJson() => {
    "Id": id == null ? null : id,
    "Code": code == null ? null : code,
    "Name": name == null ? null : name,
    "NumberOfClass": numberOfClass == null ? null : numberOfClass,
  };
}

class Professor {
  int id;
  String email;
  String firstName;
  String lastName;
  String password;
  int groupId;
  int roleId;
  DateTime lastUpdated;

  Professor({
    this.id,
    this.email,
    this.firstName,
    this.lastName,
    this.password,
    this.groupId,
    this.roleId,
    this.lastUpdated,
  });

  factory Professor.fromJson(Map<String, dynamic> json) => new Professor(
    id: json["Id"] == null ? null : json["Id"],
    email: json["Email"] == null ? null : json["Email"],
    firstName: json["FirstName"] == null ? null : json["FirstName"],
    lastName: json["LastName"] == null ? null : json["LastName"],
    password: json["Password"] == null ? null : json["Password"],
    groupId: json["GroupId"] == null ? null : json["GroupId"],
    roleId: json["RoleId"] == null ? null : json["RoleId"],
    lastUpdated: json["LastUpdated"] == null
        ? null
        : DateTime.parse(json["LastUpdated"]),
  );

  Map<String, dynamic> toJson() => {
    "Id": id == null ? null : id,
    "Email": email == null ? null : email,
    "FirstName": firstName == null ? null : firstName,
    "LastName": lastName == null ? null : lastName,
    "Password": password == null ? null : password,
    "GroupId": groupId == null ? null : groupId,
    "RoleId": roleId == null ? null : roleId,
    "LastUpdated":
    lastUpdated == null ? null : lastUpdated.toIso8601String(),
  };
}

class Room {
  int id;
  String name;
  int capacity;

  Room({
    this.id,
    this.name,
    this.capacity,
  });

  factory Room.fromJson(Map<String, dynamic> json) => new Room(
    id: json["Id"] == null ? null : json["Id"],
    name: json["Name"] == null ? null : json["Name"],
    capacity: json["Capacity"] == null ? null : json["Capacity"],
  );

  Map<String, dynamic> toJson() => {
    "Id": id == null ? null : id,
    "Name": name == null ? null : name,
    "Capacity": capacity == null ? null : capacity,
  };
}


class TimeSlot {
  int id;
  String name;
  int time;

  TimeSlot({
    this.id,
    this.name,
    this.time,
  });

  factory TimeSlot.fromJson(Map<String, dynamic> json) => new TimeSlot(
    id: json["Id"] == null ? null : json["Id"],
    name: json["Name"] == null ? null : json["Name"],
    time: json["Time"] == null ? null : json["Time"],
  );

  Map<String, dynamic> toJson() => {
    "Id": id == null ? null : id,
    "Name": name == null ? null : name,
    "Time": time == null ? null : time,
  };
}