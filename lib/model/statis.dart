// To parse this JSON data, do
//
//     final welcome = welcomeFromJson(jsonString);

import 'dart:convert';

Welcome welcomeFromJson(String str) => Welcome.fromJson(json.decode(str));

String welcomeToJson(Welcome data) => json.encode(data.toJson());

class Welcome {
  List<Report> reports;

  Welcome({
    this.reports,
  });

  factory Welcome.fromJson(Map<String, dynamic> json) => new Welcome(
    reports: json["Reports"] == null ? null : new List<Report>.from(json["Reports"].map((x) => Report.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "Reports": reports == null ? null : new List<dynamic>.from(reports.map((x) => x.toJson())),
  };
}

class Report {
  int allClasses;
  int finishedClasses;
  int absentClasses;
  int presentClasses;
  List<DateTime> present;
  List<dynamic> absent;
  List<DateTime> all;
  List<DateTime> finish;

  Report({
    this.allClasses,
    this.finishedClasses,
    this.absentClasses,
    this.presentClasses,
    this.present,
    this.absent,
    this.all,
    this.finish,
  });

  factory Report.fromJson(Map<String, dynamic> json) => new Report(
    allClasses: json["AllClasses"] == null ? null : json["AllClasses"],
    finishedClasses: json["FinishedClasses"] == null ? null : json["FinishedClasses"],
    absentClasses: json["AbsentClasses"] == null ? null : json["AbsentClasses"],
    presentClasses: json["PresentClasses"] == null ? null : json["PresentClasses"],
    present: json["Present"] == null ? null : new List<DateTime>.from(json["Present"].map((x) => DateTime.parse(x))),
    absent: json["Absent"] == null ? null : new List<dynamic>.from(json["Absent"].map((x) => x)),
    all: json["All"] == null ? null : new List<DateTime>.from(json["All"].map((x) => DateTime.parse(x))),
    finish: json["Finish"] == null ? null : new List<DateTime>.from(json["Finish"].map((x) => DateTime.parse(x))),
  );

  Map<String, dynamic> toJson() => {
    "AllClasses": allClasses == null ? null : allClasses,
    "FinishedClasses": finishedClasses == null ? null : finishedClasses,
    "AbsentClasses": absentClasses == null ? null : absentClasses,
    "PresentClasses": presentClasses == null ? null : presentClasses,
    "Present": present == null ? null : new List<dynamic>.from(present.map((x) => x.toIso8601String())),
    "Absent": absent == null ? null : new List<dynamic>.from(absent.map((x) => x)),
    "All": all == null ? null : new List<dynamic>.from(all.map((x) => x.toIso8601String())),
    "Finish": finish == null ? null : new List<dynamic>.from(finish.map((x) => x.toIso8601String())),
  };
}