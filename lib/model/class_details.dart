class ClassDetails{
  int status;
  String moduleName;
  String timeString;
  String professorName;
  String roomName;
  String dayString;
  String noteString;

  ClassDetails({this.status, this.moduleName, this.timeString,
      this.professorName, this.roomName, this.dayString, this.noteString});
}