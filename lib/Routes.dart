import 'package:acb_uni_student/Screens/Home/index.dart';
import 'package:acb_uni_student/Screens/Login/index.dart';
import 'package:acb_uni_student/model/menu_data.dart';
import 'package:acb_uni_student/screens/Attendance/attendance_bloc.dart';
import 'package:acb_uni_student/screens/Attendance/attendance_ui.dart';
import 'package:acb_uni_student/screens/Login/login_bloc.dart';
import 'package:acb_uni_student/screens/Settings/settings_ui.dart';
import 'package:acb_uni_student/screens/Statis/statis_bloc.dart';
import 'package:acb_uni_student/screens/Statis/statis_ui.dart';
import 'package:acb_uni_student/screens/TimeTablePro/timetable_ui_pro.dart';
import 'package:acb_uni_student/screens/Timeable/timeable_ui.dart';
import 'package:base_utils/bloc/bloc_providers.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

class Routes {
  Routes() {
    runApp(new MaterialApp(
      title: "Dribbble Animation App",
      debugShowCheckedModeBanner: false,
      localizationsDelegates: const <LocalizationsDelegate<MaterialLocalizations>>[
        GlobalMaterialLocalizations.delegate
      ],
      supportedLocales: const <Locale>[
        const Locale('vi', ''),
        //  const Locale('fr', ''),
      ],
      home:
      BlocProvider<LoginBloc>(
          builder: () =>LoginBloc(),
          child: LoginScreen()),
  //      HomeScreen(),
    //    TimeableScreen(),
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case '/login':
            return new MyCustomRoute(
              builder: (_) => BlocProvider<LoginBloc>(
                  builder: () =>LoginBloc(),
                  child: LoginScreen()),
              settings: settings,
            );

          case '/home':
            return new MyCustomRoute(
              builder: (_) => new HomeScreen(),
              settings: settings,
            );
          case MenuType.TIME_ABLE:
            return new MyCustomRoute(
              builder: (_) => new TimeableScreen(),
              settings: settings,
            );
          case MenuType.TIME_ABLE_PRO:
            return new MyCustomRoute(
              builder: (_) => new TimetableProScreen(),
              settings: settings,
            );
          case MenuType.CHECK_ATEN:
            return new MyCustomRoute(
              builder: (_) => BlocProvider<AttendanceBloc>(
                  builder: () =>AttendanceBloc(),
                  child: AttendanceScreen()),
              settings: settings,
            );

          case MenuType.STATIS:
            return new MyCustomRoute(
              builder: (_) => BlocProvider<StatisBloc>(
                  builder: () =>StatisBloc(),
                  child: StatisScreen()),
              settings: settings,
            );
          case MenuType.SETTINGS:
            return new MyCustomRoute(
              builder: (_) =>
                  Settings(),
              settings: settings,
            );
        }
      },
    ));
  }
}

class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({WidgetBuilder builder, RouteSettings settings})
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation, Widget child) {
    if (settings.isInitialRoute) return child;
    return new FadeTransition(opacity: animation, child: child);
  }
}
