import 'package:flutter/material.dart';

class buildDialog {
  static void showDialogNotication(BuildContext context, String message) {
    showDialog(
      context: context,
      child: new AlertDialog(
        title: new Text(message),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('OK'),
          ),
        ],
      ),
    );
  }
}
