import 'package:acb_uni_student/widget/loaders/color_loader_3.dart';
import 'package:flutter/material.dart';


class LoadingPage extends StatefulWidget {
  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      height: double.infinity,
      color: Colors.white,
      child:   ColorLoader3(
        radius: 30.0,
        dotRadius: 10.0,
      ),

    );
  }
}
