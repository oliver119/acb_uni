

import 'package:acb_uni_student/service/student_service.dart';

class Injector {

  static final studentService = StudentService.create();

}
