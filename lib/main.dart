
import 'package:acb_uni_student/Routes.dart';
import 'package:base_utils/share_preference/share_preference_manager.dart';
import 'package:flutter/material.dart';
import 'package:timezone/timezone.dart';
import 'package:flutter/services.dart' show ByteData, rootBundle;

void main() async {
  // Comment this line to enable debug printing...
  debugPrint = (String message, {int wrapWidth}) {};

  ByteData loadedData;

  await Future.wait<void>(<Future<void>>[
    rootBundle.load('assets/timezone/2018c.tzf').then((ByteData data) {
      loadedData = data;
      print('loaded data');
    })
  ]);
  initializeDatabase(loadedData.buffer.asUint8List());
  SharePreferenceManager.initSharePreference().then((data){
    new Routes();
  });


}
