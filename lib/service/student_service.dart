import 'dart:convert';

import 'package:acb_uni_student/constant.dart';
import 'package:acb_uni_student/model/attendance.dart';
import 'package:acb_uni_student/model/timeable.dart';
import 'package:acb_uni_student/model/user_info.dart';
import 'package:base_utils/network/service.dart';
import 'package:acb_uni_student/res/strings.dart';
import 'package:base_utils/network/http_utils.dart' as httpUtils;
import 'package:dio/dio.dart';

import 'package:flutter_native_timezone/flutter_native_timezone.dart';

class StudentService extends Service {
  StudentService() : super(AssetsString.groupName);
  factory StudentService.create() => StudentService();

  @override
  void dispose() {
  }

  Future<dynamic> loginSystem(
      String pass, String user, httpUtils.DataRequest<dynamic> dataRequest) async {


    final _headers = Map<String, String>();
    _headers.addAll(Constant.JSON_HEADERS);
//    _headers['Authorization'] =
//        SenpayUserManager.instance.user.senPayUserTokenForApiDomain;

    return await makeRequest(
        method: httpUtils.Method.POST,
        host: Constant.API_URL,
        path: 'api/users/authenticate',
        header: _headers,
        params: {'username': user, 'password': pass,'grant_type': 'password'},
        onSuccess: (data) {
          var info = UserInfo.fromJson(json.decode(data));
          if (info.errorDescription==null) {
            dataRequest?.onSuccess(info);
          } else {
            dataRequest?.onFailure(info.errorDescription);
          }
        },
        onFailure: (error) {
          dataRequest?.onFailure(error);
        });
  }
  Future<dynamic> loadTimeable(
      String token,String id, httpUtils.DataRequest<dynamic> dataRequest) async {


    final _headers = Map<String, String>();
    _headers.addAll(Constant.JSON_HEADERS);
    _headers['Authorization'] = "Bearer "+
       token;

    return await makeRequest(
        method: httpUtils.Method.GET,
        host: Constant.API_URL,
        path: 'api/class/all/$id',
        header: _headers,
      //  params: {'username': user, 'password': pass,'grant_type': 'password'},
        onSuccess: (data) {
          var timeTableData= Timeable.fromJson(json.decode(data));

            dataRequest?.onSuccess(timeTableData);

        },
        onFailure: (error) {
          dataRequest?.onFailure(error);
        });
  }

  Future<dynamic> loadAttendance(
      String token,String groundId,String studentId, httpUtils.DataRequest<dynamic> dataRequest) async {


    final _headers = Map<String, String>();
    _headers.addAll(Constant.JSON_HEADERS);
    _headers['Authorization'] = "Bearer "+
        token;

    return await makeRequest(
        method: httpUtils.Method.POST,
        host: Constant.API_URL,
        path: 'api/class/studentattendancereport',
        header: _headers,
         params: {'GroupId': groundId, 'StudentId': studentId},
        onSuccess: (data) {

          var attendanceData= Attendance.fromJson(json.decode(data));

          dataRequest?.onSuccess(attendanceData);

        },
        onFailure: (error) {
          dataRequest?.onFailure(error);
        });
  }

  Future<dynamic> loadStatisPro(
      String token,String proId, httpUtils.DataRequest<dynamic> dataRequest) async {


    final _headers = Map<String, String>();
    _headers.addAll(Constant.JSON_HEADERS);
    _headers['Authorization'] = "Bearer "+
        token;

    return await makeRequest(
        method: httpUtils.Method.POST,
        host: Constant.API_URL,
        path: 'api/class/professorclassreport',
        header: _headers,
        params: {'ProfessorId': proId},
        onSuccess: (data) {

          var attendanceData= Attendance.fromJson(json.decode(data));

          dataRequest?.onSuccess(attendanceData);

        },
        onFailure: (error) {
          dataRequest?.onFailure(error);
        });
  }

  Future<dynamic> addNote(




      String token,String id,String note, httpUtils.DataRequest<dynamic> dataRequest) async {
    Map<String,String> headerMap= Map();
    headerMap["Authorization"] = "Bearer "+ token;
    headerMap["Content-Type"] = "application/x-www-form-urlencoded";
    headerMap["Accept"] = "application/json";

    BaseOptions options =  BaseOptions(
      headers: headerMap,
      baseUrl: "http://gademo123abc.somee.com/",
      connectTimeout: 5000,
      receiveTimeout: 3000,
    );
    Dio dio = new Dio(options);


    dio.put("api/class/update",data:{'Id': id,'Note':note}).then((r) {
      dataRequest?.onSuccess(r.toString());
    }).catchError((){
      dataRequest?.onFailure("loi");
    });


//    final _headers = Map<String, String>();
//    _headers.addAll(Constant.JSON_HEADERS);
//    _headers['Authorization'] = "Bearer "+
//        token;
//
//    return await makeRequest(
//        method: httpUtils.Method.PUT,
//        host: Constant.API_URL,
//        path: 'api/class/update',
//        header: _headers,
//        params: {'Id': id,'Note':note},
//        onSuccess: (data) {
//
//          dataRequest?.onSuccess(data.toString());
//
//        },
//        onFailure: (error) {
//          dataRequest?.onFailure(error);
//        });
  }

}