import 'package:acb_uni_student/res/colors.dart';
import 'package:flutter/material.dart';

class InputFieldPro extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final bool obscure;
  InputFieldPro({this.hint, this.obscure,this.controller});
  @override
  Widget build(BuildContext context) {
    return (new Container(
      decoration: new BoxDecoration(
        border: new Border(
          bottom: new BorderSide(
            width: 0.5,
            color: Colors.black45,
          ),
        ),
      ),
      child: new TextFormField(
        controller: controller,
        obscureText: obscure,
        keyboardType: TextInputType.multiline,
        maxLines: 2,
        style: const TextStyle(
          color: AssetsColor.primaryColor,
        ),
        decoration: new InputDecoration(
          border: InputBorder.none,
          hintText: hint,
          hintStyle: const TextStyle(color: AssetsColor.primaryColor, fontSize: 15.0),
          contentPadding: const EdgeInsets.only(
              top: 30.0, right: 30.0, bottom: 30.0, left: 5.0),
        ),
      ),
    ));
  }
}
