import 'package:acb_uni_student/res/colors.dart';
import 'package:flutter/material.dart';

class InputFieldArea extends StatelessWidget {
  final TextEditingController controller;
  final String hint;
  final bool obscure;
  final IconData icon;
  InputFieldArea({this.hint, this.obscure, this.icon,this.controller});
  @override
  Widget build(BuildContext context) {
    return (new Container(
      decoration: new BoxDecoration(
        border: new Border(
          bottom: new BorderSide(
            width: 0.5,
            color: Colors.black45,
          ),
        ),
      ),
      child: new TextFormField(
        controller: controller,
        obscureText: obscure,
        style: const TextStyle(
          color: AssetsColor.primaryColor,
        ),
        decoration: new InputDecoration(
          icon: new Icon(
            icon,
            color: AssetsColor.primaryColor,
          ),
          border: InputBorder.none,
          hintText: hint,
          hintStyle: const TextStyle(color: AssetsColor.primaryColor, fontSize: 15.0),
          contentPadding: const EdgeInsets.only(
              top: 30.0, right: 30.0, bottom: 30.0, left: 5.0),
        ),
      ),
    ));
  }
}
