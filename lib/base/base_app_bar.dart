import 'package:base_utils/ui/network_aware_state.dart';
import 'package:flutter/material.dart';
import 'package:base_utils/utils/widget_utils.dart';
class BaseAppBar extends StatefulWidget {
  Widget child;
  String appBartitle;
  bool hasBack;
  bool hasAppBar;

  BaseAppBar({this.child, this.appBartitle,this.hasBack,this.hasAppBar=true});

  @override
  _BaseAppBarState createState() => _BaseAppBarState();
}

class _BaseAppBarState extends NetworkAwareState<BaseAppBar> {
  GlobalKey<ScaffoldState> _key;
  @override
  Widget build(BuildContext context) {
    return
      widget.hasAppBar?
      Scaffold(
      appBar:buildAppBar(context,title: widget.appBartitle,hasBack: widget.hasBack,inScaffold: true),
      key: _key,
      body: widget.child,
    ): Scaffold(
        key: _key,
        body: widget.child,
      );
  }

  @override
  GlobalKey<ScaffoldState> getScaffoldKey() {
    // TODO: implement getScaffoldKey
    return _key;
  }

  @override
  void onDisconnected() {

  }

  @override
  void onReconnected() {

  }
}
