import 'package:acb_uni_student/base/base_app_bar.dart';
import 'package:acb_uni_student/model/manager.dart';
import 'package:acb_uni_student/model/menu_data.dart';
import 'package:base_utils/share_preference/share_preference_manager.dart';
import 'package:base_utils/utils/widget_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  HomeScreenState createState() => new HomeScreenState();
}

class HomeScreenState extends State<HomeScreen> {
  List<MenuDataItem> menuListItem = Manager.instance.getUserInfo().roleId=="3"?Manager.instance.getManagerData().menuInfo:Manager.instance.getManagerData().menuInfoPro;

  String nameFull = Manager.instance.getUserInfo().firstName +
      " " +
      Manager.instance.getUserInfo().lastName;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseAppBar(
      appBartitle: Manager.instance.getUserInfo().roleId=="3"?"Student Dashboard":"Professor Dashboard",
      hasBack: false,
      hasAppBar: true,
      child: _buildBody(),
    );
  }

  Widget _buildBody() {
    return LayoutBuilder(
        builder: (BuildContext context, BoxConstraints viewportConstraints) {
      return SingleChildScrollView(
        child: ConstrainedBox(
          constraints: BoxConstraints(
            minHeight: viewportConstraints.maxHeight,
          ),
          child: IntrinsicHeight(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              // mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Container(
                    width: double.infinity,
                    height: double.infinity,
                    alignment: Alignment.center,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            width: 150,
                            height: 150,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border: Border.all(
                                    color: Colors.yellow, width: 1.0),
                                image: DecorationImage(
                                    fit: BoxFit.fitHeight,
                                    image: NetworkImage(
                                        "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTQA-e0qNeoX2ByMe0eR_XVImDJvTjLhmb1GxPG6I90NmLwcB6h")))),
                        Padding(
                          padding: EdgeInsets.only(top: 5.0),
                          child: Text(nameFull,
                              style:
                                  TextStyle(color: Colors.blue, fontSize: 25)),
                        ),
                        Padding(
                          padding: EdgeInsets.only(top: 5.0),
                          child: Text(Manager.instance.getUserInfo().email,
                              style: TextStyle(
                                  color: Colors.blueGrey, fontSize: 18)),
                        ),
                      ],
                    ),
                  ),
                ),
                Expanded(
                    flex: 3,
                    child: Container(height: 300, child: _buildListMenuItem()))
              ],
            ),
          ),
        ),
      );
    });
  }

  Widget _buildListMenuItem() {
    return GridView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: _buildMenuItem,
      itemCount: menuListItem.length,
      gridDelegate:
          new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
    );
  }

  Widget _buildMenuItem(BuildContext context, int index) {
    return Padding(
        padding: const EdgeInsets.all(2),
        child: InkWell(
          onTap: () {
            if (menuListItem[index].id == MenuType.LOG_OUT) {
              showDialog(
                context: context,
                child: new AlertDialog(
                  title: new Text('Are you want to logout ?'),
                  actions: <Widget>[
                    new FlatButton(
                      onPressed: () => Navigator.of(context).pop(false),
                      child: new Text('No'),
                    ),
                    new FlatButton(
                      onPressed: () {

                        SharePreferenceManager.removePrefs("User");
                        SharePreferenceManager.removePrefs("Pass");
                        Navigator.pushNamed(context, "/login");
                      },
                      child: new Text('Yes'),
                    ),
                  ],
                ),
              );
            } else {
              Navigator.pushNamed(context, menuListItem[index].id);
            }
          },
          child: Card(
            //width: 150,
            //height: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  menuListItem[index].icon,
                  size: scaleHeight(context, 40),
                ),
                Text(menuListItem[index].name)
              ],
            ),
          ),
        ));
  }
}
