import 'package:acb_uni_student/base/base_app_bar.dart';
import 'package:acb_uni_student/model/attendance.dart';
import 'package:acb_uni_student/res/res.dart';
import 'package:acb_uni_student/screens/Statis/statis_bloc.dart';
import 'package:acb_uni_student/widget/chart_help.dart';
import 'package:acb_uni_student/widget/loading_page.dart';
import 'package:base_utils/bloc/bloc_providers.dart';
import 'package:base_utils/utils/widget_utils.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';
import 'package:charts_flutter/flutter.dart' as charts;
class StatisScreen extends StatefulWidget {
  @override
  _StatisScreenState createState() => _StatisScreenState();
}

class _StatisScreenState extends State<StatisScreen>
    implements AddItemDelegate {
  StatisBloc bloc;

  StatisViewModel get _attenViewModel => bloc.attendanceViewModel;

  List<Report> attendanceList = List();

  List<charts.Series> seriesList;

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<StatisBloc>(context);
    bloc.loadAttention(this);
  }

  @override
  Widget build(BuildContext context) {
    return BaseAppBar(
        appBartitle: "Attendance",
        hasAppBar: true,
        hasBack: true,
        child: StreamBuilder<StatisViewModel>(
            stream: bloc.attendanceStream,
            builder: (context, snapshot) {
              if (snapshot.hasError) {}
              if (snapshot.hasData) {
                var data = snapshot.data;

                return data.isLoading == false
                    ? _buildListMenuItem()
                    : LoadingPage();
              }
            }));
  }

  Widget _buildListMenuItem() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
      Padding(
          padding:const EdgeInsets.all(5) ,
          child: Text("Total Hour Teached in this month",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w100))),
        Expanded(
          child: Padding(
            padding: const EdgeInsets.all(20),
            child: DonutAutoLabelChart(seriesList,animate: true,)
          ),
        ),

      ],
    );
  }



  @override
  void onError(String message) {
    // TODO: implement onError
  }

  @override
  void onSuccess(data) {
    seriesList=_createSampleData(data.reports[0].allClasses-data.reports[0].finishedClasses,data.reports[0].finishedClasses);
    attendanceList = data.reports;
    // TODO: implement onSuccess
  }
  /// Create one series with sample hard coded data.
  static List<charts.Series<LinearSales, int>> _createSampleData(int value1, int value2) {
    final data = [
      new LinearSales(value1*2, "hour"),
      new LinearSales(value2*2, "hour"),
    ];

    return [
      new charts.Series<LinearSales, int>(
        id: 'Unfinished',
        domainFn: (LinearSales sales, _) => sales.value,
        measureFn: (LinearSales sales, _) => sales.value,
        data: data,
        // Set a label accessor to control the text of the arc label.
        labelAccessorFn: (LinearSales row, _) => '${row.value} Hour',
      ),

    ];
  }


/// Sample linear data type.

}
class LinearSales {
  final int value;
  final String hour;

  LinearSales(this.value, this.hour);
}