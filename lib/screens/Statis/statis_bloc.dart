import 'package:acb_uni_student/injector.dart';
import 'package:acb_uni_student/model/attendance.dart';
import 'package:acb_uni_student/model/manager.dart';
import 'package:base_utils/bloc/base_bloc.dart';
import 'package:base_utils/network/http_utils.dart';
import 'package:base_utils/utils/logging_utils.dart';
import 'package:rxdart/rxdart.dart';
class StatisBloc extends BaseBloc {
  AddItemDelegate _delegate;
  StatisViewModel attendanceViewModel;

  final _attendanceController = BehaviorSubject<StatisViewModel>();

  Observable<StatisViewModel> get attendanceStream =>
      _attendanceController.stream;

  StatisBloc() {

    attendanceViewModel = new StatisViewModel();
    _attendanceController.add(attendanceViewModel);

  }
  void loadAttention(AddItemDelegate delegate) {
    _delegate = delegate;
    attendanceViewModel.isLoading=true;
    _attendanceController.add(attendanceViewModel);
    var token = Manager.instance.getUserInfo().accessToken;
    var gId = Manager.instance.getUserInfo().groupId;
    var sId = Manager.instance.getUserInfo().id;

    Injector.studentService.loadStatisPro(token,sId,DataRequest(
      onSuccess: (data) {
        attendanceViewModel.attendanceData = data;
        attendanceViewModel.isLoading=false;
        _attendanceController.add(attendanceViewModel);
        _delegate?.onSuccess(data);
      },
      onFailure: (error) {
        attendanceViewModel.isLoading=false;
        _attendanceController.add(attendanceViewModel);
        _delegate?.onError(error);
        log("attention failed : $error");
      },
    ));
  }


}


class StatisViewModel {
  bool isLoading = false;
  Attendance attendanceData= Attendance();

}

abstract class AddItemDelegate {
  void onSuccess( Attendance attendanceData);
  void onError(String message);
}
