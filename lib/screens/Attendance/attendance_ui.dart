import 'package:acb_uni_student/base/base_app_bar.dart';
import 'package:acb_uni_student/model/attendance.dart';
import 'package:acb_uni_student/res/res.dart';
import 'package:acb_uni_student/screens/Attendance/attendance_bloc.dart';
import 'package:acb_uni_student/widget/loading_page.dart';
import 'package:base_utils/bloc/bloc_providers.dart';
import 'package:base_utils/utils/widget_utils.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/linear_percent_indicator.dart';

class AttendanceScreen extends StatefulWidget {
  @override
  _AttendanceScreenState createState() => _AttendanceScreenState();
}

class _AttendanceScreenState extends State<AttendanceScreen>
    implements AddItemDelegate {
  AttendanceBloc bloc;

  AttendanceViewModel get _attenViewModel => bloc.attendanceViewModel;

  List<Report> attendanceList = List();

  @override
  void initState() {
    super.initState();

    bloc = BlocProvider.of<AttendanceBloc>(context);
    bloc.loadAttention(this);
  }

  @override
  Widget build(BuildContext context) {
    return BaseAppBar(
        appBartitle: "Attendance",
        hasAppBar: true,
        hasBack: true,
        child: StreamBuilder<AttendanceViewModel>(
            stream: bloc.attendanceStream,
            builder: (context, snapshot) {
              if (snapshot.hasError) {}
              if (snapshot.hasData) {
                var data = snapshot.data;

                return data.isLoading == false
                    ? _buildListMenuItem()
                    : LoadingPage();
              }
            }));
  }

  Widget _buildListMenuItem() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
      Padding(
          padding:const EdgeInsets.all(5) ,
          child: Text("Total progressing",style: TextStyle(fontSize: 20,fontWeight: FontWeight.w100))),
        Padding(
          padding: const EdgeInsets.all(20),
          child: LinearPercentIndicator(
            width: scaleWidth(context,300),
            lineHeight: 20.0,
            percent:_attenViewModel.attendanceData.percentageFinishClasses/100,
            center: Text(
              "${_attenViewModel.attendanceData.percentageFinishClasses}%",
              style: new TextStyle(fontSize: 12.0),
            ),
            backgroundColor: Colors.white70,
            progressColor: Colors.blueAccent,
          ),
        ),
        Expanded(
          child: GridView.builder(
            itemBuilder: _buildMenuItem,
            shrinkWrap: true,
            itemCount: attendanceList.length,
            gridDelegate:
                new SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 1),
          ),
        ),
      ],
    );
  }

  Widget _buildMenuItem(BuildContext context, int index) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Card(
        //width: 150,
        //height: 50,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 5),
              child: Text(
                _attenViewModel.attendanceData.reports[index].module,
                style: TextStyle(fontSize: 20, color: AssetsColor.primaryColor),
              ),
            ),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 20.0, right: 10.0),
//                  child: Divider(
//                    color: Colors.grey,
//                    height: 5,
//                  )
              ),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: Text("All Classes",style: TextStyle(fontSize: 15),)),
                  flex: 2,
                ),
                Expanded(
                    flex: 3,
                    child: Align(
                        alignment: Alignment.center,
                        child: Text(attendanceList[index].allClasses.toString())))
              ],
            ),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                  child: Divider(
                    color: Colors.grey,
                    height: 5,
                  )),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: Text("Finished Classes",style: TextStyle(fontSize: 15),)),
                  flex: 2,
                ),
                Expanded(
                    flex: 3,
                    child: Align(
                        alignment: Alignment.center,
                        child: Text(attendanceList[index].finishedClasses.toString())))
              ],
            ),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                  child: Divider(
                    color: Colors.grey,
                    height: 5,
                  )),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: Text("Pressent",style: TextStyle(fontSize: 15),)),
                  flex: 2,
                ),
                Expanded(
                    flex: 3,
                    child: Align(
                        alignment: Alignment.center,
                        child: Text(attendanceList[index].present.length.toString())))
              ],
            ),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 20.0, right: 10.0),
                  child: Divider(
                    color: Colors.grey,
                    height: 5,
                  )),
            ),
            Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Expanded(
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: Text("Absent",style: TextStyle(fontSize: 15),)),
                  flex: 2,
                ),
                Expanded(
                    flex: 3,
                    child: Align(
                        alignment: Alignment.center,
                        child: Text(attendanceList[index].absent.length.toString())))
              ],
            ),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 20.0, right: 10.0),
//                  child: Divider(
//                    color: Colors.grey,
//                    height: 5,
//                  )
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  void onError(String message) {
    // TODO: implement onError
  }

  @override
  void onSuccess(data) {
    attendanceList = data.reports;
    // TODO: implement onSuccess
  }
}
