import 'package:acb_uni_student/injector.dart';
import 'package:acb_uni_student/model/manager.dart';
import 'package:acb_uni_student/model/user_info.dart';
import 'package:base_utils/bloc/base_bloc.dart';
import 'package:base_utils/network/http_utils.dart';
import 'package:base_utils/utils/logging_utils.dart';
import 'package:rxdart/rxdart.dart';
class LoginBloc extends BaseBloc {
  AddItemDelegate _delegate;
  LoginViewModel loginViewModel;

  final _loginController = BehaviorSubject<LoginViewModel>();

  Observable<LoginViewModel> get loginViewModelStream =>
      _loginController.stream;

  LoginBloc() {

    loginViewModel = new LoginViewModel();
    _loginController.add(loginViewModel);


  }
  void login(String pass,String user,AddItemDelegate delegate) {
    _delegate = delegate;
    loginViewModel.isLoading=true;
    _loginController.add(loginViewModel);

    Injector.studentService.loginSystem(pass,user,DataRequest(
      onSuccess: (userdata) {

        loginViewModel.userInfo = userdata;

        if(loginViewModel.userInfo.roleId=="1"){
          loginViewModel.isLoading = false;
          _loginController.add(loginViewModel);
          _delegate?.onError("Please login on your website !");
        }else{
          Manager.instance.setUserInfo(userdata);
          //  _delegate?.onSuccess();

          getTimeableData(loginViewModel.userInfo.accessToken);
        }


       // loginViewModel.isLoading=false;
      //  _loginController.add(loginViewModel);


      },
      onFailure: (error) {

        loginViewModel.isLoading=false;
        _loginController.add(loginViewModel);
        _delegate?.onError(error);
        log("login failed : $error");
      },
    ));
  }

  void getTimeableData(String token) {
    Injector.studentService.loadTimeable(token,Manager.instance.getUserInfo().id,DataRequest(
      onSuccess: (timeableData) {

        loginViewModel.isLoading=false;

        _loginController.add(loginViewModel);

        Manager.instance.setTimeable(timeableData);

        _delegate?.onSuccess();
      },
      onFailure: (error) {

        loginViewModel.isLoading=false;
        _loginController.add(loginViewModel);
        _delegate?.onError(error.toString());
        log("login failed : $error");
      },
    ));
  }
}


class LoginViewModel {
  bool isLoading = false;
  UserInfo userInfo= UserInfo();

}

abstract class AddItemDelegate {
  void onSuccess();
  void onError(String message);
}
