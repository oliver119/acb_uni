import 'package:acb_uni_student/base/base_app_bar.dart';
import 'package:acb_uni_student/components/InputFields.dart';
import 'package:acb_uni_student/screens/Login/login_bloc.dart';
import 'package:acb_uni_student/widget/loading_page.dart';
import 'package:acb_uni_student/widget/dialog.dart';
import 'package:base_utils/utils/logging_utils.dart';
import 'package:flutter/material.dart';
import 'styles.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/animation.dart';
import 'dart:async';
import '../../components/SignUpLink.dart';
import '../../components/SignInButton.dart';

import 'package:base_utils/bloc/bloc_providers.dart';
import 'package:base_utils/share_preference/share_preference_manager.dart';
import 'package:flutter/scheduler.dart' show timeDilation;

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key key}) : super(key: key);

  @override
  LoginScreenState createState() => new LoginScreenState();
}

class LoginScreenState extends State<LoginScreen>
    with TickerProviderStateMixin implements AddItemDelegate {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();
  final Color primaryColor = Color(0xFF4aa0d5);
  final Color backgroundColor=Colors.white;
  final AssetImage backgroundImage=AssetImage("assets/full-bloom.png");



  LoginBloc bloc;

  LoginViewModel get _loginViewModel => bloc.loginViewModel;

  //var animationStatus = 0;
  @override
  void initState() {
    super.initState();


    bloc = BlocProvider.of<LoginBloc>(context);

  _usernameController.text="professor@email.com";

   // _usernameController.text="aaa@gmail.com";
    //_usernameController.text="admin@email.com";
    _passwordController.text="123";


    log("User2 "+SharePreferenceManager.getPrefs().get("User").toString());
 if(  SharePreferenceManager.checkPrefsBool("User")){
   bloc.login( SharePreferenceManager.getStringPrefs("Pass"), SharePreferenceManager.getStringPrefs("User"), this);
 }

  }

  @override
  void dispose() {

    super.dispose();
  }

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      child: new AlertDialog(
        title: new Text('Are you sure?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () =>
                Navigator.pushReplacementNamed(context, "/home"),
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ??
        false;
  }

  Widget _buildFormContainer() {
    return (new Container(
      margin: new EdgeInsets.symmetric(horizontal: 20.0),
      child: new Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          new Form(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  InputFieldArea(
                    controller: _usernameController,
                    hint: "Username",
                    obscure: false,
                    icon: Icons.person_outline,
                  ),
                  InputFieldArea(
                    controller: _passwordController,
                    hint: "Password",
                    obscure: true,
                    icon: Icons.lock_outline,
                  ),
                ],
              )),
        ],
      ),
    ));
  }

  void showToast(BuildContext context, String message) {
    final snackBar = SnackBar(content: Text(message));

// Find the Scaffold in the Widget tree and use it to show a SnackBar
    Scaffold.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    bloc = BlocProvider.of<LoginBloc>(context);
    return
        BaseAppBar(
          appBartitle: "ABC CMS",
          hasAppBar: false,
          hasBack: false,
            child:
            StreamBuilder<LoginViewModel>(
                stream: bloc.loginViewModelStream,
                builder: (context, snapshot) {
                  if (snapshot.hasError) {
                    buildDialog.showDialogNotication(context, "Vui lòng kiểm tra mạng !");
                  }
                  if (snapshot.hasData) {
                    var data = snapshot.data;

                    return
                      data.isLoading == false
                          ? Container(
                          decoration: new BoxDecoration(
                           //image: backgroundImage,
                          ),
                          child: new Container(
//                              decoration: new BoxDecoration(
//                                  gradient: new LinearGradient(
//                                    colors: <Color>[
//                                      const Color.fromRGBO(
//                                          162, 146, 199, 0.8),
//                                      const Color.fromRGBO(
//                                          51, 51, 63, 0.9),
//                                    ],
//                                    stops: [0.2, 1.0],
//                                    begin: const FractionalOffset(
//                                        0.0, 0.0),
//                                    end: const FractionalOffset(
//                                        0.0, 1.0),
//                                  )),
                              child: new ListView(
                                padding: const EdgeInsets.all(0.0),
                                children: <Widget>[
                                  new Stack(
                                    alignment:
                                    AlignmentDirectional.bottomCenter,
                                    children: <Widget>[
                                      new Column(
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
//                                          new Tick(image: tick),
                                          new ClipPath(
                                            clipper: MyClipper(),
                                            child: Container(
                                              decoration: BoxDecoration(
                                                image: new DecorationImage(
                                                  image: this.backgroundImage,
                                                  fit: BoxFit.cover,
                                                ),
                                              ),
                                              alignment: Alignment.center,
                                              padding: EdgeInsets.only(top: 150.0, bottom: 100.0),
                                              child: Column(
                                                children: <Widget>[
                                                  Text(
                                                    "ABC",
                                                    style: TextStyle(
                                                        fontSize: 50.0,
                                                        fontWeight: FontWeight.bold,
                                                        color: this.primaryColor),
                                                  ),
                                                  Text(
                                                    "University CMS System",
                                                    style: TextStyle(
                                                        fontSize: 20.0,
                                                        fontWeight: FontWeight.bold,
                                                        color: this.primaryColor),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          _buildFormContainer(),
                                          new SignUp()
                                        ],
                                      ),
                                      new Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 50.0),
                                        child: new InkWell(
                                            onTap: () {
                                              if (_passwordController
                                                  .text.isEmpty ||
                                                  _usernameController
                                                      .text.isEmpty) {
                                                buildDialog.showDialogNotication(context, "UserName and Password not emtry");
                                              }else{
                                                bloc.login(
                                                    _passwordController
                                                        .text,
                                                    _usernameController
                                                        .text,this);
                                              }
                                            },
                                            child: new SignIn()),
                                      )
                                    ],
                                  ),
                                ],
                              )))
                          : LoadingPage();
                  }
                })
        );

  }

  @override
  void onError(String message) {
    buildDialog.showDialogNotication(context, message);
  }

  @override
  void onSuccess() {
   SharePreferenceManager.setPrefs("User", Type.STRING, _usernameController.text);
   SharePreferenceManager.setPrefs("Pass", Type.STRING, _passwordController.text);
    Navigator.pushReplacementNamed(context, "/home");
  }

}
class MyClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path p = new Path();
    p.lineTo(size.width, 0.0);
    p.lineTo(size.width, size.height * 0.85);
    p.arcToPoint(
      Offset(0.0, size.height * 0.85),
      radius: const Radius.elliptical(50.0, 10.0),
      rotation: 0.0,
    );
    p.lineTo(0.0, 0.0);
    p.close();
    return p;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}