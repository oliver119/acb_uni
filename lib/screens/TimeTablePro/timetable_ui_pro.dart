import 'dart:math';

import 'package:acb_uni_student/base/base_app_bar.dart';
import 'package:acb_uni_student/model/timeable.dart';
import 'package:acb_uni_student/model/manager.dart';
import 'package:acb_uni_student/res/colors.dart';
import 'package:acb_uni_student/screens/TimeTablePro/timetable_popup_pro.dart';

import 'package:acb_uni_student/widget/calendar/src/calendar.dart';
import 'package:acb_uni_student/widget/calendar/src/calendarevent.dart';
import 'package:base_utils/utils/widget_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_native_timezone/flutter_native_timezone.dart';
import 'package:timezone/timezone.dart';

class TimetableProScreen extends StatefulWidget {
  TimetableProScreen({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _TimetableProScreenState createState() => new _TimetableProScreenState();
}

class _TimetableProScreenState extends State<TimetableProScreen> {
  List<CalendarEvent> events = <CalendarEvent>[];
  Location loc;
  List<AllClasess> timeableList;
  //Random random = new Random();

  @override
  void initState() {
    super.initState();
    timeableList = List();

    timeableList = Manager.instance.getTimeable().allClasess;

  }


  Widget buildItem(BuildContext context, CalendarEvent e) {



  if (timeableList[e.index].isCanceled.isCanceled == false &&
      timeableList[e.index].isCanceled.isNewClass == false) {
    return InkWell(
      onTap: (){
        TimeableProPopup.show(context, timeableList[e.index]);
      },
      child: new Card(
        child: new ListTile(
          title: new Text(timeableList[e.index].roomId.name.toString()),
          subtitle: new Text(timeableList[e.index].timeSlotId.name.toString()),
          leading: const Icon(Icons.event_note),
        ),
      ),
    );

  } else if (timeableList[e.index].isCanceled.isCanceled == true &&
      timeableList[e.index].isCanceled.isNewClass == false) {
    return InkWell(
      onTap: (){
        TimeableProPopup.show(context, timeableList[e.index]);
      },
      child: new Card(
        child: new ListTile(
          title: new Text(timeableList[e.index].roomId.name.toString()),
          subtitle: new Text(timeableList[e.index].timeSlotId.name.toString()),
          leading: const Icon(Icons.cancel),
        ),
      ),
    );

  }else if (timeableList[e.index].isCanceled.isCanceled == true &&
      timeableList[e.index].isCanceled.isNewClass == true) {
    return InkWell(
      onTap: (){
        TimeableProPopup.show(context, timeableList[e.index]);
      },
      child: new Card(
        child: new ListTile(
          title: new Text(timeableList[e.index].roomId.name.toString()),
          subtitle: new Text(timeableList[e.index].timeSlotId.name.toString()),
          leading: const Icon(Icons.new_releases),
        ),
      ),
    );

  }

  return InkWell(
    onTap: (){
      TimeableProPopup.show(context, timeableList[e.index]);
    },
    child: new Card(
      child: new ListTile(
        title: new Text(timeableList[e.index].roomId.name.toString()),
        subtitle: new Text(timeableList[e.index].timeSlotId.name.toString()),
        leading: const Icon(Icons.event_note),
      ),
    ),
  );


  }

  List<CalendarEvent> getEvents(DateTime start, DateTime end) {
    if (loc != null && events.length == 0) {
      TZDateTime nowTime =
      new TZDateTime.now(loc).subtract(new Duration(days: 30));
      DateTime date;


      for(int i =0;i<timeableList.length;i++){
        DateTime myDatetime = DateTime.parse(timeableList[i].date.toString());

        TZDateTime start = TZDateTime.from(myDatetime, loc);
        //  nowTime.add(new Duration(days: i + random.nextInt(10)));
        events.add(new CalendarEvent(
            index: i,
            instant: start,
            instantEnd: start));
      }

    }
    return events;
  }


  Function onTimmer(){

  }

  @override
  Widget build(BuildContext context) {

    timeableList.sort((a,b)=>a.date.compareTo(b.date));

    return  BaseAppBar(
      appBartitle: "Timeable",
      hasAppBar: true,
      hasBack: true,
      child: new Column(
        children: <Widget>[
          new FutureBuilder<String>(
            future: FlutterNativeTimezone.getLocalTimezone(),
            builder: (BuildContext context, AsyncSnapshot<String> tz) {
              if (tz.hasData) {
                loc = getLocation(tz.data);
                TZDateTime nowTime = new TZDateTime.now(loc);
                return new Expanded(
                  child: new CalendarWidget(
                    initialDate: nowTime,
                    beginningRangeDate: TZDateTime.from(timeableList[0].date, loc),
                    endingRangeDate: TZDateTime.from(timeableList[timeableList.length-1].date, loc),
                    location: loc,
                    buildItem: buildItem,
                    getEvents: getEvents,
                    headerMonthStyle: TextStyle(color: AssetsColor.primaryColor,fontSize: scaleHeight(context,20)),
                    bannerHeader:
                    new AssetImage("assets/images/line2.jpg"),
                    monthHeader:
                    new AssetImage("assets/images/line3.jpg"),
                    weekBeginsWithDay: 1, // Sunday = 0, Monday = 1, Tuesday = 2, ..., Saturday = 6
                  ),
                );
              } else {
                return new Center(
                  child: new Text("Getting the timezone..."),
                );
              }
            },
          ),
        ],
      ),
    );
  }

}