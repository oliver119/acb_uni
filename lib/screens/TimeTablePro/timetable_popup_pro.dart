import 'package:acb_uni_student/components/InputFields.dart';
import 'package:acb_uni_student/components/inputPro.dart';
import 'package:acb_uni_student/injector.dart';
import 'package:acb_uni_student/model/class_details.dart';
import 'package:acb_uni_student/model/manager.dart';
import 'package:acb_uni_student/model/timeable.dart';
import 'package:acb_uni_student/res/colors.dart';
import 'package:acb_uni_student/utils/string_utils.dart';
import 'package:acb_uni_student/widget/dialog_manager.dart';
import 'package:base_utils/network/http_utils.dart';
import 'package:base_utils/utils/date_utils.dart';
import 'package:base_utils/utils/widget_utils.dart';
import 'package:base_utils/notifications/notifications.dart';
import 'package:flutter/material.dart';
import 'package:acb_uni_student/widget/dialog.dart';
import 'package:acb_uni_student/res/strings.dart';
import 'package:base_utils/share_preference/share_preference_manager.dart';

class TimeableProPopup extends StatefulWidget {
  AllClasess mClasses;


//  Function onClickLeftFun;
//  Function onClickRightFun;

  TimeableProPopup({this.mClasses});

  static void show(BuildContext context, AllClasess classes) {
    DialogManager.show(
        context: context,
        barrierColor: Colors.black54,
        builder: (context) => TimeableProPopup(mClasses: classes));
  }

  @override
  _TimeableProPopupState createState() => _TimeableProPopupState();
}

class _TimeableProPopupState extends State<TimeableProPopup> {
  double screenW;
  double screenH;
  ClassDetails classDetails;
  bool isShowTimerBtn = true;
  final _noteController = TextEditingController();

  @override
  void initState() {
    super.initState();
    classDetails = getStatusClass();

    if(widget.mClasses.date.isAfter(DateTime.now())&&classDetails.status==0){
     isShowTimerBtn = true;
    }else{
      isShowTimerBtn = false;
    }

  }

  @override
  Widget build(BuildContext context) {
    screenW = MediaQuery.of(context).size.width;
    screenH = MediaQuery.of(context).size.height;
    return Container(
      width: screenW - 60,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Container(
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(8)),
              color: Colors.white,
            ),
            padding: EdgeInsets.all(17),
            child: _buildContentOnly(),
          ),
        ],
      ),
    );
  }

  Widget _buildContentOnly() {
    return Column(
      children: <Widget>[
        SizedBox(height: scaleHeight(context, 20)),
        Text(
          // builder.content,
          "Class Details",
          textAlign: TextAlign.center,
          style: TextStyle(
              fontSize: scaleHeight(context, 23),
              fontWeight: FontWeight.w700,
              color: AssetsColor.primaryColor),
        ),
        SizedBox(height: scaleHeight(context, 10)),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(
                flex: 1,
                child: Align(
                    alignment: Alignment.centerRight,
                    child: Text(
                      "Module",
                      style: TextStyle(
                          color: AssetsColor.primaryColor,
                          fontSize: scaleHeight(context, 20)),
                    ))),
            Expanded(
                flex: 3,
                child: Padding(
                  padding: EdgeInsets.only(left: 10.0),
                  child: Text(
                    classDetails.moduleName,
                    style: TextStyle(
                        color: AssetsColor.subColor,
                        fontSize: scaleHeight(context, 20)),
                  ),
                )),
          ],
        ),
        Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        "Time",
                        style: TextStyle(
                            color: AssetsColor.primaryColor,
                            fontSize: scaleHeight(context, 20)),
                      ))),
              Expanded(
                  flex: 3,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(
                      classDetails.timeString,
                      style: TextStyle(
                          color: AssetsColor.subColor,
                          fontSize: scaleHeight(context, 20)),
                    ),
                  )),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        "Professor",
                        style: TextStyle(
                            color: AssetsColor.primaryColor,
                            fontSize: scaleHeight(context, 20)),
                      ))),
              Expanded(
                  flex: 3,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(
                      classDetails.professorName,
                      style: TextStyle(
                          color: AssetsColor.subColor,
                          fontSize: scaleHeight(context, 20)),
                    ),
                  )),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        "Room",
                        style: TextStyle(
                            color: AssetsColor.primaryColor,
                            fontSize: scaleHeight(context, 20)),
                      ))),
              Expanded(
                  flex: 3,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(
                      classDetails.roomName,
                      style: TextStyle(
                          color: AssetsColor.subColor,
                          fontSize: scaleHeight(context, 20)),
                    ),
                  )),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        "Date",
                        style: TextStyle(
                            color: AssetsColor.primaryColor,
                            fontSize: scaleHeight(context, 20)),
                      ))),
              Expanded(
                  flex: 3,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Text(
                      classDetails.dayString,
                      style: TextStyle(
                          color: AssetsColor.subColor,
                          fontSize: scaleHeight(context, 20)),
                    ),
                  )),
            ],
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top: 8.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(
                  flex: 1,
                  child: Align(
                      alignment: Alignment.centerRight,
                      child: Text(
                        "Note",
                        style: TextStyle(
                            color: AssetsColor.primaryColor,
                            fontSize: scaleHeight(context, 20)),
                      ))),
              Expanded(
                  flex: 3,
                  child: Padding(
                    padding: EdgeInsets.only(left: 10.0),
                    child: Container(
                      width: double.infinity,
                      height: 50,
                      child: StringUtils.checkDateNow(widget.mClasses.date)
                          ? Row(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                Expanded(
                                  flex: 3,
                                  child: InputFieldPro(
                                    controller: _noteController,
                                    hint: "Add Note",
                                    obscure: false,
                                    // icon: Icons.person_outline,
                                  ),
                                ),
                                Expanded(
                                  flex: 2,
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: IconButton(
                                        icon: Icon(
                                          Icons.add,
                                          size: 25,
                                        ),
                                        onPressed: () {
                                          addNote(widget.mClasses.id.toString(),
                                              _noteController.text);
                                        }),
                                  ),
                                )
                              ],
                            )
                          : Text(
                              classDetails.noteString,
                              style: TextStyle(
                                  color: AssetsColor.subColor,
                                  fontSize: scaleHeight(context, 20)),
                            ),
                    ),
                  )),
            ],
          ),
        ),
        classDetails.status != 0
            ? Padding(
                padding: EdgeInsets.only(top: 8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                        flex: 1,
                        child: Align(
                            alignment: Alignment.centerRight,
                            child: Text(
                              "Attention",
                              style: TextStyle(
                                  color: AssetsColor.primaryColor,
                                  fontSize: scaleHeight(context, 20)),
                            ))),
                    Expanded(
                        flex: 3,
                        child: Padding(
                          padding: EdgeInsets.only(left: 10.0),
                          child: Text(
                            classDetails.status == 1
                                ? "This class is cancel"
                                : "This class is new",
                            style: TextStyle(
                                color: AssetsColor.subColor,
                                fontSize: scaleHeight(context, 20)),
                          ),
                        )),
                  ],
                ),
              )
            : Container(),
        classDetails.status == 0
            ? Container()
            : InkWell(
                onTap: () {
                  setState(() {
                    classDetails.status == 2
                        ? classDetails = getDataAttentionClass(2)
                        : classDetails = getDataAttentionClass(1);
                  });
                },
                child: Text(
                  classDetails.status == 1
                      ? "View old Class"
                      : "View new class",
                  style: TextStyle(color: AssetsColor.primaryColor),
                )),
        SizedBox(height: scaleHeight(context, 20)),
        Container(
          //  color: Colors.white,
          child: GridView.count(
              physics: NeverScrollableScrollPhysics(),
              crossAxisCount: 2,
              shrinkWrap: true,
              //mainAxisSpacing: 10,
              crossAxisSpacing: 17,
              childAspectRatio: 156 / 48,
              children: <Widget>[
                // button left
                isShowTimerBtn==false?Container():
                _buildButtons(Colors.white, true, AssetsColor.subColor, "Timer",
                    AssetsColor.primaryColor, "", () {
//                  widget.onClickLeftFun();

                  setNotification();
                }),

                // button right
                _buildButtons(AssetsColor.primaryColor, false,
                    Colors.transparent, "OK", Colors.white, "", () {
                  // widget.onClickRightFun();
                }),
              ]),
        ),
        SizedBox(height: scaleHeight(context, 20)),
      ],
    );
  }

  Widget _buildButtons(Color background, bool isBorder, Color borderColor,
      String title, Color titleColor, String icon, Function onTap) {
    final buttonHeight =
        ((screenW - 60 - (17 * 3)) / 2) * (48 / 156).toDouble();
    return InkWell(
      onTap: () {
        Navigator.of(context).pop();
        onTap();
      },
      child: Container(
          decoration: BoxDecoration(
            color: background,
            borderRadius: BorderRadius.all(Radius.circular(buttonHeight / 2)),
            border: Border.all(width: isBorder ? 1 : 0, color: borderColor),
          ),
          child: Center(
              child: Text(title,
                  style: TextStyle(
                      color: titleColor,
                      fontSize: scaleHeight(context, 18),
                      fontWeight: FontWeight.w600)))

//        Text(title,
//            style: TextStyle(
//                color: titleColor,fontSize: scaleHeight(context, 21))
//        ),
          ),
    );
  }

  ClassDetails getStatusClass() {
    ClassDetails result = new ClassDetails();
    if (widget.mClasses.isCanceled.isCanceled == false &&
        widget.mClasses.isCanceled.isNewClass == false) {
      result = ClassDetails(
          status: 0,
          moduleName: widget.mClasses.moduleId.name,
          timeString: widget.mClasses.timeSlotId.name,
          professorName: widget.mClasses.professorId.firstName +
              " " +
              widget.mClasses.professorId.lastName,
          roomName: widget.mClasses.roomId.name,
          dayString:
              StringUtils.getDatefromDatetimeString(widget.mClasses.date),
          noteString: "");
    } else if (widget.mClasses.isCanceled.isCanceled == true &&
        widget.mClasses.isCanceled.isNewClass == false) {
      result = ClassDetails(
          status: 1,
          moduleName: widget.mClasses.moduleId.name,
          timeString: widget.mClasses.timeSlotId.name,
          professorName: widget.mClasses.professorId.firstName +
              " " +
              widget.mClasses.professorId.lastName,
          roomName: widget.mClasses.roomId.name,
          dayString:
              StringUtils.getDatefromDatetimeString(widget.mClasses.date),
          noteString: "");
    } else if (widget.mClasses.isCanceled.isCanceled == true &&
        widget.mClasses.isCanceled.isNewClass == true) {
      result = ClassDetails(
          status: 2,
          moduleName: widget.mClasses.moduleId.name,
          timeString: widget.mClasses.timeSlotId.name,
          professorName: widget.mClasses.professorId.firstName +
              " " +
              widget.mClasses.professorId.lastName,
          roomName: widget.mClasses.roomId.name,
          dayString:
              StringUtils.getDatefromDatetimeString(widget.mClasses.date),
          noteString: widget.mClasses.note!=null?widget.mClasses.note.toString():"");
    }
    return result;
  }

  ClassDetails getDataAttentionClass(int type) {
    ClassDetails result = new ClassDetails();
    if (type == 2) {
      result = ClassDetails(
          status: 1,
          moduleName: widget.mClasses.isCanceled.oldClass.moduleName,
          timeString: widget.mClasses.isCanceled.oldClass.timeSlotName,
          professorName: widget.mClasses.isCanceled.oldClass.professorName,
          roomName: widget.mClasses.isCanceled.oldClass.roomName,
          dayString: StringUtils.getDatefromDatetimeString(
              widget.mClasses.isCanceled.oldClass.date),
          noteString: widget.mClasses.note!=null?widget.mClasses.note.toString():"");
    } else if (type == 1) {
      result = ClassDetails(
          status: 2,
          moduleName: widget.mClasses.isCanceled.newClass.moduleName,
          timeString: widget.mClasses.isCanceled.newClass.timeSlotName,
          professorName: widget.mClasses.isCanceled.newClass.professorName,
          roomName: widget.mClasses.isCanceled.newClass.roomName,
          dayString: StringUtils.getDatefromDatetimeString(
              widget.mClasses.isCanceled.newClass.date),
          noteString: widget.mClasses.note!=null?widget.mClasses.note.toString():"");
    }
    return result;
  }

  void addNote(String id, String note) {
    Injector.studentService.addNote(
        Manager.instance.getUserInfo().accessToken,
        id,
        note,
        DataRequest(
          onSuccess: (userdata) {
            buildDialog.showDialogNotication(context, "Adding note success !");
          },
          onFailure: (error) {
            buildDialog.showDialogNotication(
                context, "Adding note fails.Please try later !");
          },
        ));
  }

  void setNotification() {
    bool isS = true;
    bool isV = true;

    if (SharePreferenceManager.getPrefsBool(AssetsString.set_vira) != null) {
      isV = SharePreferenceManager.getPrefsBool(AssetsString.set_vira);
    }
    if (SharePreferenceManager.getPrefsBool(AssetsString.set_sound) != null) {
      isS = SharePreferenceManager.getPrefsBool(AssetsString.set_sound);
    }

    DateTime now = DateTime.now();

    int timer = widget.mClasses.date.millisecondsSinceEpoch +
        (int.parse(StringUtils.getTime(widget.mClasses.timeSlotId.name)) *
            60 *
            60 *
            1000);

    var timeSet;
    if (SharePreferenceManager.getStringPrefs(AssetsString.set_time) != null) {
      timeSet = (int.parse(
              SharePreferenceManager.getStringPrefs(AssetsString.set_time)) *
          60 *
          1000);
    } else {
      timeSet = (30 * 60 * 1000);
    }

    var temp = timer - now.millisecondsSinceEpoch - timeSet;

    NotificationManager.initial().show(
      title: widget.mClasses.moduleId.name +
          " class will start in  ${SharePreferenceManager.getStringPrefs(AssetsString.set_time)} minutes.",
      body: widget.mClasses.timeSlotId.name+ " at "+widget.mClasses.roomId.name,
      afterTime: temp,
      notificationId: widget.mClasses.id,
      isVira: isV,
      isSoundf: isS
    );
  }
}
