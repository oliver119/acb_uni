import 'package:acb_uni_student/base/base_app_bar.dart';
import 'package:acb_uni_student/res/colors.dart';
import 'package:acb_uni_student/res/strings.dart';
import 'package:base_utils/share_preference/share_preference_manager.dart';
import 'package:flutter/material.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:acb_uni_student/widget/dialog.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  int _currentValue = 30;
  bool _checkVibrate = false;
  bool _checkSound = true;

  @override
  void initState() {
    if (SharePreferenceManager.getPrefsBool(AssetsString.set_vira) != null) {
      _checkVibrate =
          SharePreferenceManager.getPrefsBool(AssetsString.set_vira);
    }
      if (SharePreferenceManager.getPrefsBool(AssetsString.set_sound) != null) {
        _checkSound =
            SharePreferenceManager.getPrefsBool(AssetsString.set_sound);
      }

      if (SharePreferenceManager.getStringPrefs(AssetsString.set_time) !=
          null) {
        _currentValue = int.parse( SharePreferenceManager.getStringPrefs(AssetsString.set_time));

      }

  }

  @override
  Widget build(BuildContext context) {
    return BaseAppBar(
        appBartitle: "Timer Settings",
        hasAppBar: true,
        hasBack: true,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Minuter timer :",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w100),
                    ),
                  )),
                  Expanded(
                      child: Align(
                    alignment: Alignment.center,
                    child: InkWell(
                        onTap: () {
                          _showDialog();
                        },
                        child: Text(
                          _currentValue.toString(),
                          style: TextStyle(fontSize: 20),
                        )),
                  ))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Vibrate :",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w100),
                    ),
                  )),
                  Expanded(
                      child: Align(
                          alignment: Alignment.center,
                          child: InkWell(
                              onTap: () {
                                _showDialog();
                              },
                              child: Checkbox(
                                value: _checkVibrate,
                                onChanged: (bool newValue) {
                                  setState(() {
                                    _checkVibrate = newValue;
                                  });
                                },
                              ))))
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: Row(
                children: <Widget>[
                  Expanded(
                      child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Sound :",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w100),
                    ),
                  )),
                  Expanded(
                      child: Align(
                          alignment: Alignment.center,
                          child: InkWell(
                              onTap: () {
                                _showDialog();
                              },
                              child: Checkbox(
                                value: _checkSound,
                                onChanged: (bool newValue) {
                                  setState(() {
                                    _checkSound = newValue;
                                  });
                                },
                              )))),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: InkWell(
                onTap: () {
                  SharePreferenceManager.setPrefs(AssetsString.set_time, Type.STRING, _currentValue.toString());
                  SharePreferenceManager.setPrefs(AssetsString.set_sound, Type.BOOL, _checkSound);
                  SharePreferenceManager.setPrefs(AssetsString.set_vira, Type.BOOL, _checkVibrate);

                  buildDialog.showDialogNotication(context, "Save successful !");
                },
                child: Container(
                  width: 250.0,
                  height: 50.0,
                  alignment: FractionalOffset.center,
                  decoration: new BoxDecoration(
                    color: AssetsColor.primaryColor,
                    borderRadius:
                        new BorderRadius.all(const Radius.circular(30.0)),
                  ),
                  child: new Text(
                    "Save",
                    style: new TextStyle(
                      color: Colors.white,
                      fontSize: 18.0,
                      fontWeight: FontWeight.w300,
                      letterSpacing: 0.3,
                    ),
                  ),
                ),
              ),
            )
          ],
        ));
  }

  void _showDialog() {
    showDialog<int>(
        context: context,
        builder: (BuildContext context) {
          return new NumberPickerDialog.integer(
            minValue: 10,
            maxValue: 60,
            title: new Text("Pick a minuter"),
            initialIntegerValue: _currentValue,
          );
        }).then((int value) {
      if (value != null) {
        setState(() => _currentValue = value);
      }
    });
  }
}
