import 'package:intl/intl.dart';

class StringUtils {
  static String getDatefromDatetimeString(DateTime date) {
    String formattedDate = DateFormat('dd-MM-yyyy').format(date);

    return formattedDate;
  }

  static bool checkDateNow(DateTime date) {
    if (date.isAfter(DateTime.now())) {
      return true;
    }
    return false;
  }

  static String getTime(String timeSlot){
    List<String> string= timeSlot.split(":");
    return string[0];
  }
}
